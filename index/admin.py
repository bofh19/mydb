from django.contrib import admin
from django import forms
from ajax_select import make_ajax_form
from ajax_select.admin import AjaxSelectAdmin

from index.models import featured

class featuredAdmin(AjaxSelectAdmin):
	search_fields=['movie_id__movie_name']
	form = make_ajax_form(featured,{'movie_id':'movies_lookups','person_id':'movie_person_lookups'})


admin.site.register(featured,featuredAdmin)