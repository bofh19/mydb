# Create your views here.
from django.shortcuts import render_to_response
from django.template import RequestContext

from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse


def add_page(request):
	return render_to_response('index/templates/add.html',
								{
								},context_instance=RequestContext(request)
							)
def list_page(request):
	return render_to_response('index/templates/list.html',
								{
								},context_instance=RequestContext(request)
							)

from movies.models import movies
from movie_trailers.models import movie_trailers
from images.models import banner_images
import random
from movie_genres.models import movie_genres
from genres.models import genres
from movie_persons.models import movie_persons
from movie_casting.models import movie_casting
from professions.models import professions
from movies.models import movie_theater_status
from images.models import profile_images
from images.models import background_images
from index.models import featured

def main_page(request):
	global_back_image=random.choice(background_images.objects.all())
	if request.user.is_authenticated():
		last_login = request.session.get('social_auth_last_login_backend')
	else:
		last_login=''
	try:
		back_images=background_images.objects.all()#.order_by('-id')[:10]
		back_image=random.choice(back_images)
	except Exception, e:
		back_image=''
	featured_movies=[]
	featured_persons=[]
	try:
		featured_movies=featured.objects.filter(movie_id__isnull=False).order_by('-id')[:4]
	except Exception, e:
		print e
		featured_movies=[]
	try:
		featured_persons=featured.objects.filter(person_id__isnull=False).order_by('-id')[:4]
	except Exception, e:
		featured_persons=[]
	
	return render_to_response('index/templates/main_page.html',
								{
								'last_login':last_login,
								'back_image':back_image,
								'movies':featured_movies,
								'persons':featured_persons,
								},context_instance=RequestContext(request)
							)


def latest_theaters(request):
	global_back_image=random.choice(background_images.objects.all())
	if request.user.is_authenticated():
		last_login = request.session.get('social_auth_last_login_backend')
	else:
		last_login=''
	movies_in_theater=movie_theater_status.objects.filter(release_status='T')
	latest_movies_list=[]
	for movie in movies_in_theater:
		latest_movies_list.append(movies.objects.get(id=movie.movie_id.id))
	latest_movies_list=latest_movies_list[::-1]
	latest_movies=[]
	
	for movie in latest_movies_list:
		movie_json={}
		movie_json['id']=movie

		now=date.today()
		diff=now-movie.movie_year
		movie_json['days_left']=diff.days
		
		try:
			genres=movie_genres.objects.filter(movie_id=movie.id)
		except UnboundLocalError:
			genres="Does Not Exist"
		except movie_genres.DoesNotExist:
			genres="Does Not Exist"

		movie_json['genres']=genres

		try:
			movie_per=[]
			movie_pers=[]

			prof_id=professions.objects.get(profession_name='Hero')
			this_movie_persons=movie_casting.objects.filter(movie_id=movie.id,profession_id=prof_id.id)
			
			for person in this_movie_persons:
				movie_per.append(movie_persons.objects.get(id=person.movie_persons_id.id))

			prof_id=professions.objects.get(profession_name='Heroine')
			this_movie_persons=(movie_casting.objects.filter(movie_id=movie.id,profession_id=prof_id.id))
			for person in this_movie_persons:
				movie_per.append(movie_persons.objects.get(id=person.movie_persons_id.id))

			prof_id=professions.objects.get(profession_name='Director')
			this_movie_persons=(movie_casting.objects.filter(movie_id=movie.id,profession_id=prof_id.id))
			for person in this_movie_persons:
				movie_per.append(movie_persons.objects.get(id=person.movie_persons_id.id))

			prof_id=professions.objects.get(profession_name='Music Director')
			this_movie_persons=(movie_casting.objects.filter(movie_id=movie.id,profession_id=prof_id.id))
			for person in this_movie_persons:
				movie_per.append(movie_persons.objects.get(id=person.movie_persons_id.id))

			prof_id=professions.objects.get(profession_name='Producer')
			this_movie_persons=(movie_casting.objects.filter(movie_id=movie.id,profession_id=prof_id.id))
			 		
			for person in this_movie_persons:
				movie_per.append(movie_persons.objects.get(id=person.movie_persons_id.id))

			movie_per=list(set(movie_per))

			for person in movie_per:
				person_json={}
				person_json['id']=person
				try:
					person_json['profile_image']=random.choice(profile_images.objects.filter(person_id=person.id,image_choosen='T'))
				except Exception, e:
					person_json['profile_image']=''
				movie_pers.append(person_json)
			movie_json['persons']=movie_pers
		except Exception, e:

			movie_json['persons']=''



		try:
			this_trailers=movie_trailers.objects.filter(movie_id=movie.id)[::-1]
			movie_json['trailer']=this_trailers[0]
		except Exception, e:
			movie_json['trailer']=''
		try:
			this_movie_banner_images=banner_images.objects.filter(movie_id=movie.id)
			this_movie_banner_image=random.choice(this_movie_banner_images)
		except banner_images.DoesNotExist:
			this_movie_banner_image=''
		except:
			this_movie_banner_image=''

		movie_json['banner_image']=this_movie_banner_image
		latest_movies.append(movie_json)
	latest_movies=sortwithdays(latest_movies)
	
	return render_to_response('index/templates/latest_theaters.html',
								{
								'last_login':last_login,
								'movies_list':latest_movies,
							#	'back_image':global_back_image,
								},context_instance=RequestContext(request)
							)

from django.http import Http404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

def latest_db(request):
	global_back_image=random.choice(background_images.objects.all())
	items_per_page=20
	
	if request.user.is_authenticated():
		last_login = request.session.get('social_auth_last_login_backend')
	else:
		last_login=''
	try:
		if request.GET['p']:
			page_number=int(request.GET['p'])
		else:
			page_number=1
	except Exception, e:
		page_number=1
	page=page_number
	latest_movies_list_all=movies.objects.all()[::-1]
	#numberofpages=int(len(latest_movies_list_all)/items_per_page)+1
	#if page_number>numberofpages:
	#	raise Http404
	#latest_movies_list=latest_movies_list_all[(page_number-1)*items_per_page:page_number*items_per_page]
	#if latest_movies_list==[]:
	#	latest_movies_list=(latest_movies_list_all[::-1])[0:items_per_page]	
	
	paginator = Paginator(latest_movies_list_all,items_per_page)
	try:
		latest_movies_list = paginator.page(page)
	except PageNotAnInteger:
		latest_movies_list = paginator.page(1)
	except EmptyPage:
		latest_movies_list = paginator.page(paginator.num_pages)

	latest_movies=[]
	for movie in latest_movies_list:
		movie_json={}
		movie_json['id']=movie

		try:
			genres=movie_genres.objects.filter(movie_id=movie.id)
		except UnboundLocalError:
			genres="Does Not Exist"
		except movie_genres.DoesNotExist:
			genres="Does Not Exist"

		movie_json['genres']=genres

		try:
			movie_per=[]
			movie_pers=[]

			prof_id=professions.objects.get(profession_name='Hero')
			this_movie_persons=movie_casting.objects.filter(movie_id=movie.id,profession_id=prof_id.id)
			
			for person in this_movie_persons:
				movie_per.append(movie_persons.objects.get(id=person.movie_persons_id.id))

			prof_id=professions.objects.get(profession_name='Heroine')
			this_movie_persons=(movie_casting.objects.filter(movie_id=movie.id,profession_id=prof_id.id))
			for person in this_movie_persons:
				movie_per.append(movie_persons.objects.get(id=person.movie_persons_id.id))

			prof_id=professions.objects.get(profession_name='Director')
			this_movie_persons=(movie_casting.objects.filter(movie_id=movie.id,profession_id=prof_id.id))
			for person in this_movie_persons:
				movie_per.append(movie_persons.objects.get(id=person.movie_persons_id.id))

			prof_id=professions.objects.get(profession_name='Music Director')
			this_movie_persons=(movie_casting.objects.filter(movie_id=movie.id,profession_id=prof_id.id))
			for person in this_movie_persons:
				movie_per.append(movie_persons.objects.get(id=person.movie_persons_id.id))

			prof_id=professions.objects.get(profession_name='Producer')
			this_movie_persons=(movie_casting.objects.filter(movie_id=movie.id,profession_id=prof_id.id))
			 		
			for person in this_movie_persons:
				movie_per.append(movie_persons.objects.get(id=person.movie_persons_id.id))

			movie_per=list(set(movie_per))

			for person in movie_per:
				person_json={}
				person_json['id']=person
				try:
					person_json['profile_image']=random.choice(profile_images.objects.filter(person_id=person.id,image_choosen='T'))
				except Exception, e:
					person_json['profile_image']=''
				movie_pers.append(person_json)
			movie_json['persons']=movie_pers
		except Exception, e:

			movie_json['persons']=''


		try:
			this_trailers=movie_trailers.objects.filter(movie_id=movie.id)[::-1]
			movie_json['trailer']=this_trailers[0]
		except Exception, e:
			movie_json['trailer']=''
		try:
			this_movie_banner_images=banner_images.objects.filter(movie_id=movie.id)
			this_movie_banner_image=random.choice(this_movie_banner_images)
		except banner_images.DoesNotExist:
			this_movie_banner_image=''
		except:
			this_movie_banner_image=''
		movie_json['banner_image']=this_movie_banner_image
		latest_movies.append(movie_json)
	return render_to_response('index/templates/latest_db.html',
								{
								'last_login':last_login,
								'movies_list':latest_movies,
								'page_list':latest_movies_list,
							#	'back_image':global_back_image,
								},context_instance=RequestContext(request)
							)



def latest_trailers(request):
	global_back_image=random.choice(background_images.objects.all())
	items_per_page=10
	if request.user.is_authenticated():
		last_login = request.session.get('social_auth_last_login_backend')
	else:
		last_login=''
	
	try:
		if request.GET['p']:
			page_number=int(request.GET['p'])
		else:
			page_number=1
	except Exception, e:
		page_number=1
	page=page_number
	this_trailers=movie_trailers.objects.all()[::-1]
	paginator = Paginator(this_trailers,items_per_page)

	try:
		latest_trailers = paginator.page(page)
	except PageNotAnInteger:
		latest_trailers = paginator.page(1)
	except EmptyPage:
		latest_trailers = paginator.page(paginator.num_pages)

	#
	#latest_trailers=this_trailers[:10]
	return render_to_response('index/templates/latest_trailers.html',
								{
								'last_login':last_login,
								'latest_trailers':latest_trailers,
								'page_list':latest_trailers,
							#	'back_image':global_back_image,
								},context_instance=RequestContext(request)
							)
def sortwithdays(a):
	x=len(a)
	i=0
	while(x>1):
		i=0
		while(i<(x-1)):
			if a[i]['days_left']>a[i+1]['days_left']:
				b=a[i]
				a[i]=a[i+1]
				a[i+1]=b
			i=i+1
		x=x-1
	return a

from datetime import *

def releasing(request):
	global_back_image=random.choice(background_images.objects.all())
	if request.user.is_authenticated():
		last_login = request.session.get('social_auth_last_login_backend')
	else:
		last_login=''
	movies_in_theater=movie_theater_status.objects.filter(release_status='D')
	latest_movies_list=[]
	for movie in movies_in_theater:
		latest_movies_list.append(movies.objects.get(id=movie.movie_id.id))
	latest_movies_list=latest_movies_list[::-1]
	latest_movies=[]
	
	for movie in latest_movies_list:
		movie_json={}
		movie_json['id']=movie
		now=date.today()
		diff=movie.movie_year-now
		movie_json['days_left']=diff.days
		if movie.movie_year.year == 1800:
			movie_json['days_left'] = 1800
		
		if diff.days <= 0 :
			print "ERROR RELEASE STATUS AT movie id - "+ str(movie.id)+ ' movie-name '+str(movie.movie_name)

		try:
			genres=movie_genres.objects.filter(movie_id=movie.id)
		except UnboundLocalError:
			genres="Does Not Exist"
		except movie_genres.DoesNotExist:
			genres="Does Not Exist"

		movie_json['genres']=genres

		try:
			movie_per=[]
			movie_pers=[]

			prof_id=professions.objects.get(profession_name='Hero')
			this_movie_persons=movie_casting.objects.filter(movie_id=movie.id,profession_id=prof_id.id)
			
			for person in this_movie_persons:
				movie_per.append(movie_persons.objects.get(id=person.movie_persons_id.id))

			prof_id=professions.objects.get(profession_name='Heroine')
			this_movie_persons=(movie_casting.objects.filter(movie_id=movie.id,profession_id=prof_id.id))
			for person in this_movie_persons:
				movie_per.append(movie_persons.objects.get(id=person.movie_persons_id.id))

			prof_id=professions.objects.get(profession_name='Director')
			this_movie_persons=(movie_casting.objects.filter(movie_id=movie.id,profession_id=prof_id.id))
			for person in this_movie_persons:
				movie_per.append(movie_persons.objects.get(id=person.movie_persons_id.id))

			prof_id=professions.objects.get(profession_name='Music Director')
			this_movie_persons=(movie_casting.objects.filter(movie_id=movie.id,profession_id=prof_id.id))
			for person in this_movie_persons:
				movie_per.append(movie_persons.objects.get(id=person.movie_persons_id.id))

			prof_id=professions.objects.get(profession_name='Producer')
			this_movie_persons=(movie_casting.objects.filter(movie_id=movie.id,profession_id=prof_id.id))
			 		
			for person in this_movie_persons:
				movie_per.append(movie_persons.objects.get(id=person.movie_persons_id.id))

			movie_per=list(set(movie_per))

			for person in movie_per:
				person_json={}
				person_json['id']=person
				try:
					person_json['profile_image']=random.choice(profile_images.objects.filter(person_id=person.id,image_choosen='T'))
				except Exception, e:
					person_json['profile_image']=''
				movie_pers.append(person_json)
			movie_json['persons']=movie_pers
		except Exception, e:

			movie_json['persons']=''



		try:
			this_trailers=movie_trailers.objects.filter(movie_id=movie.id)[::-1]
			movie_json['trailer']=this_trailers[0]
		except Exception, e:
			movie_json['trailer']=''
		try:
			this_movie_banner_images=banner_images.objects.filter(movie_id=movie.id)
			this_movie_banner_image=random.choice(this_movie_banner_images)
		except banner_images.DoesNotExist:
			this_movie_banner_image=''
		except:
			this_movie_banner_image=''

		movie_json['banner_image']=this_movie_banner_image
		latest_movies.append(movie_json)
	latest_movies=sortwithdays(latest_movies)
	back_image=random.choice(background_images.objects.all())
	return render_to_response('index/templates/releasing.html',
								{
								'last_login':last_login,
								'movies_list':latest_movies,
							#	'back_image':global_back_image,
								},context_instance=RequestContext(request)
							)