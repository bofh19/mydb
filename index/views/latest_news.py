# Create your views here.
from django.shortcuts import render_to_response
from django.template import RequestContext

from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse
from movie_news.models import movie_news
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

def latest_news(request):
	items_per_page=20
	if request.user.is_authenticated():
		last_login = request.session.get('social_auth_last_login_backend')
	else:
		last_login=''
	
	try:
		if request.GET['p']:
			page_number=int(request.GET['p'])
		else:
			page_number=1
	except Exception, e:
		page_number=1

	page=page_number
	
	this_news=movie_news.objects.all()[::-1]
	paginator = Paginator(this_news,items_per_page)

	try:
		latest_news = paginator.page(page)
	except PageNotAnInteger:
		latest_news = paginator.page(1)
	except EmptyPage:
		latest_news = paginator.page(paginator.num_pages)
	
	return render_to_response('index/templates/latest_news.html',
								{
								'last_login':last_login,
								'latest_news':latest_news,
								},context_instance=RequestContext(request)
							)