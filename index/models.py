from django.db import models

# Create your models here.
from movies.models import movies
from movie_persons.models import movie_persons

class featured(models.Model):
	movie_id=models.ForeignKey(movies,null=True,blank=True,unique=True)
	person_id=models.ForeignKey(movie_persons,null=True,blank=True,unique=True)

	def __unicode__(self):
		output=''
		try:
			output=output+self.movie_id.movie_name
		except Exception, e:
			pass
		try:
			output=output+self.person_id.person_name
		except Exception, e:
			pass
		return output