# Create your views here.
from django.shortcuts import render_to_response
from django.template import RequestContext

from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse


def add_page(request):
	return render_to_response('index/templates/add.html',
								{
								},context_instance=RequestContext(request)
							)
def list_page(request):
	return render_to_response('index/templates/list.html',
								{
								},context_instance=RequestContext(request)
							)

def main_page(request):
	if request.user.is_authenticated():
		ctx = {	
        'last_login': request.session.get('social_auth_last_login_backend')
   				 }
		return render_to_response('index/templates/main_page.html',ctx,context_instance=RequestContext(request))
	else:
		return render_to_response('index/templates/main_page.html',
								{
								},context_instance=RequestContext(request)
							)