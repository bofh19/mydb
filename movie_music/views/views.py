# Create your views here.\
from django.http import HttpResponse
from movie_music.models import song_links
from movie_music.models import movie_music
from urlparse import urlparse
from django.shortcuts import render_to_response
from django.template import RequestContext
from images.models import background_images
import random
def music_index(request):
	try:
		back_image= random.choice(background_images.objects.all()[::-1][:5])
	except Exception, e:
		back_image=''
	return render_to_response('movie_music/templates/music_index.html',
								{
								'back_image':back_image,
								},context_instance=RequestContext(request)
						)

def ajaxaddurl(request):
	if request.user.is_authenticated():
		username=request.user.username
		try:
			url=request.POST['url']
			if url=='':
				return HttpResponse("please enter url")
		except Exception, e:
			return HttpResponse("please enter url")
		
		new_song_link=song_links()
		
		try:
			new_song_link.song_id=movie_music.objects.get(id=request.POST['song_id'])
		except Exception, e:
			return HttpResponse('something went wrong while searching for song')

		try:
			new_song_link.link_type=request.POST['link_type']
#			print new_song_link.link_type
			if new_song_link.link_type==None  or new_song_link.link_type=='undefined':
				return HttpResponse('please select link type Audio or Video')
		except Exception, e:
			return HttpResponse('something went wrong while entering link type')

		try:
			new_song_link.link_loc_type=request.POST['link_loc_type']
		except Exception, e:
			print e
			return HttpResponse("something went wrong while calculation link loc type")

		
		new_song_link.song_url=request.POST['url']

		try:
			new_song_link.save()
		except Exception, e:
			return HttpResponse('something went wrong while saving the url')
		
		return HttpResponse("saved the url reload to see the result")
	
	else:
		return HttpResponse("please login to do this")
	return HttpResponse(request)


##
#
#
#

#
#
#
#
#

#
#
#
#
#

#
#
import os
import random
from settings import MEDIA_ROOT
def music_list_local_dir(request):
	path=MEDIA_ROOT+'/music'

	structure=[]
	for name in os.listdir(path):
		this_dir={}
		if os.path.isdir(os.path.join(path,name)):
			this_dir['dirname']=name
			this_dir_files={}
			this_dir_file_mp3=[]
			this_dir_file_jpg=[]
			for each_file in os.listdir(os.path.join(path,name)):
				if os.path.isfile(os.path.join(path,name,each_file)):					
					filename,fileext=os.path.splitext(os.path.join(path,name,each_file))
					if fileext=='.mp3' or fileext=='.MP3' or fileext=='.wav' or fileext=='.WAV' or fileext=='.ogg':
						this_dir_file_mp3.append(each_file)
					elif fileext=='.jpg' or fileext=='.png' or fileext=='.jpeg'or fileext=='.JPG'or fileext=='.PNG'or fileext=='.GIF'or fileext=='.gif':
						this_dir_file_jpg.append(each_file)
					
			this_dir_files['mp3']=this_dir_file_mp3
			this_dir_files['jpg']=this_dir_file_jpg

			this_dir['files']=this_dir_files
		structure.append(this_dir)
	return render_to_response('movie_music/templates/music_list_local_dir.html',
								{
								'files':structure,
								},context_instance=RequestContext(request)
						)












