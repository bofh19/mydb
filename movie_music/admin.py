from movie_music.models import movie_music
from movie_music.models import song_persons_info, song_video_info
from django.contrib import admin
from movie_music.models import song_links
from movie_music.models import songs_loc
from movie_music.models import cd_images


from ajax_select import make_ajax_form
from ajax_select.admin import AjaxSelectAdmin

class song_persons_infoInline(admin.TabularInline):
	model=song_persons_info
	form = make_ajax_form(song_persons_info,{'person_id':'movie_person_lookups'})
	extra=1

class song_linksInline(admin.TabularInline):
	model=song_links
	extra=1

class song_video_infoInline(admin.TabularInline):
	model=song_video_info
	form = make_ajax_form(song_video_info,{'person_id':'movie_person_lookups'})
	extra=1

class songs_loc_Inline(admin.TabularInline):
	model=songs_loc
	extra=1

class movie_musicAdmin(AjaxSelectAdmin):
    form = make_ajax_form(movie_music,{'movie_id':'movies_lookups'})
    inlines=[songs_loc_Inline,song_linksInline,song_persons_infoInline,song_video_infoInline]

class cd_imagesAdmin(AjaxSelectAdmin):
    form = make_ajax_form(cd_images,{'movie_id':'movies_lookups'})

admin.site.register(movie_music,movie_musicAdmin)
admin.site.register(song_persons_info)
admin.site.register(song_video_info)
admin.site.register(song_links)
admin.site.register(songs_loc)
admin.site.register(cd_images,cd_imagesAdmin)