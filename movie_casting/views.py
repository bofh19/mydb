# Create your views here.
from django.http import HttpResponseRedirect, HttpResponse
from django.template import RequestContext
from django.shortcuts import render_to_response
from movie_casting.forms import addcast_form
from movie_persons.models import movie_persons
from movies.models import movies
from movie_casting.models import movie_casting
from professions.models import professions
from movie_characters.models import movie_characters
from django.core.urlresolvers import reverse

def addcast(request,movie_id):
	#verifying is staff status here
	if request.user.is_authenticated():
		if request.user.is_staff:
			username=request.user.username
		else:
			return HttpResponse("user not staff click back on browser to go back")
	else:
		return HttpResponse("""login before to <a href=/user/login/>login here</a>""")
		
	form = addcast_form()
	try:
		movie_name=movies.objects.get(id=movie_id).movie_name
	except:
		return HttpResponse("Error 1")
	return render_to_response("movie_casting/templates/enter_cast.html",
									{
									'movie_name':movie_name,
									'movie_id':movie_id,
									'form':form,
									},
									context_instance=RequestContext(request))


def addingcast(request,movie_id):
	#verifying is staff status here
	if request.user.is_authenticated():
		if request.user.is_staff:
			username=request.user.username
		else:
			return HttpResponse("user not staff click back on browser to go back")
	else:
		return HttpResponse("""login before to <a href=/user/login/>login here</a>""")
	
	movie_name=movies.objects.get(id=movie_id).movie_name
	f=addcast_form(request.POST)
	if f.is_valid():
		output="x"
	else:
		return HttpResponse("Not Valid Data")
	if request.method=='POST':
		new_cast=movie_casting()
		try:
			new_person=movie_persons.objects.get(person_name__iexact=f.cleaned_data['person_name'])
		except:
			return HttpResponse("Error Near Person")
		try:
			new_person_profession=professions.objects.get(profession_desc__iexact=f.cleaned_data['person_profession'])
		except:
			return HttpResponse("Error Near Person Profession")			
		if f.cleaned_data['character_name'] == '':
			pass
		else:
			print "it entered"
			new_char=movie_characters()
			new_char.character_name=f.cleaned_data['character_name']
			new_char.movie_id=movies.objects.get(id=movie_id)
			new_char.movie_persons_id=new_person
			try:
				new_char.save()
			except:
				return HttpResponse("Error Near saving character")
		
		new_cast.movie_persons_id=new_person
		new_cast.movie_id=movies.objects.get(id=movie_id)
		new_cast.profession_id=new_person_profession
		try:
			new_cast.save()
		except:
			try:
				new_char.delete()
			except:
				pass
			return HttpResponse("Error Near saving new cast may be not unique")
	else:
		return HttpResponse("ERROR 2")
	movie_name="-"+movie_name
	return HttpResponseRedirect(reverse('movie_casting.views.addcast',  args=(movie_id,movie_name)))