from django import template

from movie_casting.models import movie_casting
import random
from sorl.thumbnail import get_thumbnail

register = template.Library()

class getStatusNode(template.Node):
	def __init__(self,movie_id,person_id):
		self.movie_id=movie_id
		self.person_id=person_id
	def render(self,context):
		try:
			out = movie_casting.objects.filter(movie_id=self.movie_id,movie_persons_id=self.person_id)
			#print out
			if out:
				return '1'
		except Exception, e:
			#print e
			return ''


@register.simple_tag
def get_cast_status(movie_id,person_id):
	#print movie_id
	#print person_id
	return getStatusNode(movie_id,person_id)

