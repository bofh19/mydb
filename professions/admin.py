from professions.models import professions
from django.contrib import admin

admin.site.register(professions)