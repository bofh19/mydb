from django import forms
from django.contrib.admin import widgets 
import datetime
from professions.models import professions
class add_prof_form(forms.Form):
	profession_name=forms.CharField(max_length=40)
	profession_catagory=forms.CharField(max_length=40,initial='Movie')
	profession_level=forms.IntegerField()
	profession_desc=forms.CharField(max_length=40)
	
	def clean_profession_name(self):
		profession_name = self.cleaned_data['profession_name']
		test_profession=professions()
		try:
			test_profession=professions.objects.get(profession_name=profession_name)
		except test_profession.DoesNotExist:
			return profession_name
		raise forms.ValidationError(u'%s already exists' % profession_name)
