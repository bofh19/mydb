# Create your views here.
import json
from django.shortcuts import get_object_or_404
from django.template import RequestContext
from django.http import HttpResponse
from django.utils.datastructures import MultiValueDictKeyError
from professions.models import professions
from django.shortcuts import render_to_response

def profession_searching(request):
	if request.is_ajax():
		q=request.GET.get('term','')
		professionz=professions.objects.filter(profession_desc__icontains=q)[:20]
		results=[]
		for profession in professionz:
			profession_json={}
			profession_json['id']=profession.id
			profession_json['label']=profession.profession_desc
			profession_json['value']=profession.profession_desc
			results.append(profession_json)
		data=json.dumps(results)
	else:
		data='fail'
	mimetype='application/json'
	return HttpResponse(data, mimetype)


from professions.forms import add_prof_form
def add_prof(request):
	#verifying is staff status here
	if request.user.is_authenticated():
		if request.user.is_staff:
			username=request.user.username
		else:
			return HttpResponse("user not staff click back on browser to go back")
	else:
		return HttpResponse("""login before to <a href=/user/login/>login here</a>""")
		
	added_profession=''
	if request.method=='POST':
		if request.user.is_authenticated():
			username=request.user.username
		else:
			return HttpResponse("""login before to tag <a href=/movie/index/>login here</a>""")
		f=add_prof_form(request.POST)
		if f.is_valid():
			newprofession=professions()
			newprofession.profession_name=f.cleaned_data['profession_name']
			newprofession.profession_catagory=f.cleaned_data['profession_catagory']
			newprofession.profession_level=f.cleaned_data['profession_level']
			newprofession.profession_desc=f.cleaned_data['profession_desc']
			try:
				newprofession.save()
			except Exception, e:
				print e
				return HttpResponse("failed to save")
			added_profession=newprofession.profession_name
	else:
		f=add_prof_form()
	return render_to_response("professions/templates/add_prof.html",
									{
									'form':f,
									'added_profession': added_profession,
									},
									context_instance=RequestContext(request))
	