
from django.db.models import Q
from django.utils.html import escape

from ajax_select import LookupChannel

from professions.models import professions


class professionsLookup(LookupChannel):

    model = professions

    def get_query(self,q,request):
        return professions.objects.filter(Q(profession_name__icontains=q) | Q(profession_desc__icontains=q)).order_by('profession_name')

    def get_result(self,obj):
        u""" result is the simple text that is the completion of what the person typed """
        return obj.profession_name

    def format_match(self,obj):
        """ (HTML) formatted item for display in the dropdown """
        return self.format_item_display(obj)

    def format_item_display(self,obj):
        """ (HTML) formatted item for displaying item in the selected deck area """
        return u"%s -- %s -- %s <hr/>" % (escape(obj.profession_name),escape(str(obj.profession_level)),escape(obj.profession_desc))
