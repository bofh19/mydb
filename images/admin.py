from images.models import images, background_images,banner_images, profile_images
from django.contrib import admin
from images.models import images_tags
from images.models import image_seq


from sorl.thumbnail import default
ADMIN_THUMBS_SIZE = '160x120'
BANNER_THUMBS_SIZE = '100x100'
"""
class imagesAdmin(admin.ModelAdmin):
    model = images
    
    list_display = ['image_thumb', 'user_id', 'image_desc','tag_enabled' ]

    def image_thumb(self, obj):
        if obj.imagefile:
            thumb = default.backend.get_thumbnail(obj.imagefile.file, ADMIN_THUMBS_SIZE)
            return u'<img width="%s" src="%s" />' % (thumb.width, thumb.url)
        else:
            return "No Image" 
    image_thumb.short_description = 'My Thumbnail'
    image_thumb.allow_tags = True
"""

class background_imagesAdmin(admin.ModelAdmin):
    model = background_images
    search_fields=['movie_id__movie_name']
    list_display = ['image_thumb', 'movie_id' ]

    def image_thumb(self, obj):
        if obj.imagefile:
            try:
                thumb = default.backend.get_thumbnail(obj.imagefile.file, ADMIN_THUMBS_SIZE)
                return u'<img width="%s" src="%s" />' % (thumb.width, thumb.url)
            except Exception, e:
                return "cant crop"
        else:
            return "No Image" 
    image_thumb.short_description = 'My Thumbnail'
    image_thumb.allow_tags = True

class banner_imagesAdmin(admin.ModelAdmin):
    model = background_images
    search_fields=['movie_id__movie_name']
    list_display = ['image_thumb', 'movie_id' ]

    def image_thumb(self, obj):
        if obj.imagefile:
            try:
                thumb = default.backend.get_thumbnail(obj.imagefile.file, BANNER_THUMBS_SIZE)
                return u'<img width="%s" src="%s" />' % (thumb.width, thumb.url)
            except Exception, e:
                return "cant crop"
        else:
            return "No Image" 
    image_thumb.short_description = 'My Thumbnail'
    image_thumb.allow_tags = True

class profile_imagesAdmin(admin.ModelAdmin):
    model = background_images
    search_fields=['person_id__person_name']
    list_display = ['image_thumb', 'person_id' ]

    def image_thumb(self, obj):
        if obj.imagefile:
            try:
                thumb = default.backend.get_thumbnail(obj.imagefile.file, BANNER_THUMBS_SIZE)
                return u'<img width="%s" src="%s" />' % (thumb.width, thumb.url)
            except Exception, e:
                return "cant crop"
        else:
            return "No Image" 
    image_thumb.short_description = 'My Thumbnail'
    image_thumb.allow_tags = True


admin.site.register(images)
admin.site.register(background_images,background_imagesAdmin)
admin.site.register(banner_images,banner_imagesAdmin)
admin.site.register(profile_images,profile_imagesAdmin)
admin.site.register(image_seq)
admin.site.register(images_tags)