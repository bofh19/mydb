
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect,HttpResponse
from images.models import background_images
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

def back_images_all(request):
	items_per_page=50
	if request.user.is_authenticated():
		last_login = request.session.get('social_auth_last_login_backend')
	else:
		last_login=''
	try:
		if request.GET['p']:
			page_number=int(request.GET['p'])
		else:
			page_number=1
	except Exception, e:
		page_number=1

	page=page_number
	
	this_images=background_images.objects.all()[::-1]
	paginator = Paginator(this_images,items_per_page)

	try:
		latest_images = paginator.page(page)
	except PageNotAnInteger:
		latest_images = paginator.page(1)
	except EmptyPage:
		latest_images = paginator.page(paginator.num_pages)
	
	return render_to_response('images/templates/back_images_all.html',
								{
								'last_login':last_login,
								'all_images':latest_images,
								},context_instance=RequestContext(request)
							)

def back_image(request,image_id):
	
	items_per_page=1
	if request.user.is_authenticated():
		last_login = request.session.get('social_auth_last_login_backend')
	else:
		last_login=''
	

	
	next_image=''
	previous_image=''
	this_image=background_images.objects.get(id=image_id)
	all_images=background_images.objects.all()
	total_num=len(all_images)
	break_factor=0
	current_num=0
	
	for each_image in all_images:
		current_num=current_num+1
		if break_factor==1:
			next_image=each_image
			break
		if each_image==this_image:
			break_factor=1
		else:
			previous_image=each_image
	
	
	return render_to_response('images/templates/back_image.html',
								{
								'last_login':last_login,
								'this_image':this_image,
								'next':previous_image,
								'previous':next_image,
								'total_num':total_num,
								'current_num':current_num,
								},context_instance=RequestContext(request)
							)