# Create your views here.
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect,HttpResponse
from images.models import images_tags
from images.models import images
from images.models import image_seq
from images.forms import *
from django.core.urlresolvers import reverse
from movies.models import movies
from movie_persons.models import movie_persons
from images.models import background_images
MEDIA_SERVER_URL="http://127.0.0.1:8000"
def image_movie_view(request,movie_id,image_id):
	try:
		this_image=images.objects.get(id=image_id)
		this_movie=movies.objects.get(id=movie_id)
	except:
		return HttpResponse("does not exist")

	tags_persons=[]
	tags_type=[]
	tags_movie=[]
	try:
		all_tags=images_tags.objects.filter(image_id=image_id)
	except:
		all_tags=''
	try:
		this_image_seq=image_seq.objects.get(image_id=image_id)
		all_images_seq=image_seq.objects.filter(movie_id=this_image_seq.movie_id)
		x=0
		for a in all_images_seq:
			if a.image_id.id==this_image_seq.image_id.id:
				if a.movie_id.id==this_image_seq.movie_id.id:
					break;
			x=x+1
		if(x>0):
			previous_image=images.objects.get(id=all_images_seq[x-1].image_id.id)
		else:
			previous_image=''

		if(x<(len(all_images_seq))):
			try:
				next_image=images.objects.get(id=all_images_seq[x+1].image_id.id)
			except Exception, e:
				next_image=''
		else:
			next_image=''
	except Exception, e:
		#print "Exception "
		#print e
		next_image=''
		previous_image=''
	for a in all_tags:
		try:
			tags_persons.append(a.person_id)
			if (a==None):
				pass
			else:
				tags_type.append(a.image_type)
			tags_type=list(set(tags_type))
			tags_movie.append(a.movie_id)
			tags_movie=list(set(tags_movie))
			tags_characters.append(a.character_id)
			tags_characters=list(set(tags_characters))
		except:
			pass
	#print tags_persons
	return render_to_response(
		'images/templates/image_movie_view.html',
		{'image':this_image,
		'movie_id':movie_id,
		 'tags_movie':tags_movie,
		 'tags_type':tags_type,
		 'tags_persons':tags_persons,
		 'next':next_image,
		 'previous':previous_image,
		 'name':this_movie.movie_name,
		},
		context_instance=RequestContext(request)
)

def image_movie_view_all(request,movie_id):
	try:
		this_movie=movies.objects.get(id=movie_id)

		name=this_movie.movie_name
		
	except Exception, e:
		this_movie=''
		this_movie_images=''
		return render_to_response(
		'images/templates/image_movie_view_all.html',
		{},
		context_instance=RequestContext(request))

	try:
		this_movie_imagez=image_seq.objects.filter(movie_id=movie_id)
		total_images=len(this_movie_imagez)
		if len(this_movie_imagez)>1	:
			this_movie_imagez=this_movie_imagez[::-1]
		this_movie_images=[]
		for a in this_movie_imagez:
			this_movie_images.append(images.objects.get(id=a.image_id.id))
	except Exception, e:
		this_movie_images=[]

	try:
		this_movie_back_imgages=background_images.objects.filter(movie_id=movie_id)
	except Exception, e:
		this_movie_back_imgages=''
	return render_to_response(
		'images/templates/image_movie_view_all.html',
		{'images':this_movie_images,
		 'movie_id':movie_id,
		 'name':name,
		 'total_images':total_images,
		 'background_images':this_movie_back_imgages,
		},
		context_instance=RequestContext(request)
)

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
def back_image_movie_view(request,movie_id,image_id):
	next_image=''
	previous_image=''

	if request.user.is_authenticated():
		last_login = request.session.get('social_auth_last_login_backend')
	else:
		last_login=''
	
	try:
		this_image=background_images.objects.get(id=image_id)
		this_movie=movies.objects.get(id=movie_id)
	except:
		this_image=''
		this_movie=''

		return render_to_response('images/templates/back_image_movie_view.html',{
										'last_login':last_login,
										'image':this_image,
										'movie_id':movie_id,
										'next':next_image,
										'previous':previous_image,
					},
						context_instance=RequestContext(request))

	
	all_images=background_images.objects.filter(movie_id=movie_id)
	break_factor=0
	
	for each_image in all_images:
		if break_factor==1:
			next_image=each_image
			break
		if each_image==this_image:
			break_factor=1
		else:
			previous_image=each_image
	
	
	return render_to_response(
		'images/templates/back_image_movie_view.html',
		{
		'last_login':last_login,
		'image':this_image,
		'movie_id':movie_id,
		'next':next_image,
		'previous':previous_image,
		'name':this_movie.movie_name,
		},
		context_instance=RequestContext(request))

def back_image_movie_view_all(request,movie_id):
	try:
		this_movie=movies.objects.get(id=movie_id)

		name=this_movie.movie_name
		
	except Exception, e:
		this_movie=''
		this_movie_images=''
		return render_to_response(
		'images/templates/image_movie_view_all.html',
		{},
		context_instance=RequestContext(request))

	this_movie_images=''
	total_images=''
	try:
		this_movie_back_imgages=background_images.objects.filter(movie_id=movie_id)
	except Exception, e:
		this_movie_back_imgages=''
	return render_to_response(
		'images/templates/back_image_movie_view_all.html',
		{
		'images':this_movie_images,
		 'movie_id':movie_id,
		 'name':name,
		 'total_images':total_images,
		 'background_images':this_movie_back_imgages,
		},
		context_instance=RequestContext(request)
)
