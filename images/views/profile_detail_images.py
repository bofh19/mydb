# Create your views here.
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect,HttpResponse
from images.models import images_tags
from images.models import images
from images.models import image_seq
from images.forms import *
from django.core.urlresolvers import reverse
from movies.models import movies

from movie_persons.models import movie_persons
from images.models import profile_images
MEDIA_SERVER_URL="http://127.0.0.1:8000"

def profile_detail_image(request,person_id,image_id):
	try:
		this_image=profile_images.objects.get(id=image_id)
		this_person=movie_persons.objects.get(id=person_id)
	except:
		return HttpResponse("does not exist")
	##################################################
	return render_to_response(
		'images/templates/profile_detail_image.html',
		{'image':this_image,
		 'name':this_person,
		},
		context_instance=RequestContext(request)
)
