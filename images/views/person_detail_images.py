# Create your views here.
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect,HttpResponse
from images.models import images_tags
from images.models import images
from images.models import image_seq
from images.forms import *
from django.core.urlresolvers import reverse
from movies.models import movies

from movie_persons.models import movie_persons
from images.models import profile_images
MEDIA_SERVER_URL="http://127.0.0.1:8000"

def person_detail_images(request,person_id):
	try:
		this_person=movie_persons.objects.get(id=person_id)
		name=this_person
	except Exception, e:
		this_person=''
		this_person_images=''

	try:
		this_person_imagez=images_tags.objects.filter(person_id=person_id)
		if len(this_person_imagez)>1:
			this_person_imagez=this_person_imagez[::-1]
		this_person_images=[]
		for a in this_person_imagez:
			this_person_images.append(images.objects.get(id=a.image_id.id))
	except Exception, e:
		this_person_images=[]

	try:
		this_person_profile_imagez=profile_images.objects.filter(person_id=person_id)
		this_person_profile_images=[]
		for x in this_person_profile_imagez:
			this_person_profile_images.append(x)
	except Exception, e:
		this_person_profile_images=[]
	return render_to_response(
		'images/templates/person_detail_images.html',
		{'images':this_person_images,
		 'person_id':person_id,
		 'name':name,
		 'profile_images':this_person_profile_images,
		},
		context_instance=RequestContext(request))


def person_detail_image(request,person_id,image_id):
	try:
		this_image=images.objects.get(id=image_id)
		this_person=movie_persons.objects.get(id=person_id)
	except:
		return HttpResponse("does not exist")

	tags_persons=[]
	tags_type=[]
	tags_movie=[]
	try:
		all_tags=images_tags.objects.filter(image_id=image_id)
	except:
		all_tags=''
	for a in all_tags:
		try:
			tags_persons.append(a.person_id)
			if (a.image_type==None):
				pass
			else:
				tags_type.append(a.image_type)
			tags_type=list(set(tags_type))
			tags_movie.append(a.movie_id)
			tags_movie=list(set(tags_movie))
			tags_characters.append(a.character_id)
			tags_characters=list(set(tags_characters))
		except:
			pass
	################### NExt and Previous ################
	try:
		this_person_all_tags_images=images_tags.objects.filter(person_id=person_id)
	except Exception, e:
		pass
	if len(this_person_all_tags_images)>1:
		this_person_all_tags_images=this_person_all_tags_images[::-1]
	this_person_all_images=[]
	for a in this_person_all_tags_images:
		this_person_all_images.append(images.objects.get(id=a.image_id.id))
	next=''
	previous=''
	count=0
	for x in this_person_all_images:
		if int(x.id)==int(image_id):
			try:
				previous=this_person_all_images[count-1]
				next=this_person_all_images[count+1]
			except Exception, e:
				pass
			break
		else:
			pass
			#print "x.id="+str(x.id)
			#print "image_id="+str(image_id)
		count=count+1
	##################################################
	return render_to_response(
		'images/templates/person_detail_image.html',
		{'image':this_image,
		 'tags_movie':tags_movie,
		 'tags_type':tags_type,
		 'tags_persons':tags_persons,
		 'name':this_person,
		 'next':next,
		 'previous':previous,
		},
		context_instance=RequestContext(request)
)
