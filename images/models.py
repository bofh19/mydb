from django.db import models
import os
import uuid
import datetime
from movies.models import movies
from movie_persons.models import movie_persons
from movie_characters.models import movie_characters
from django.contrib.auth.models import User
# Create your models here.
def get_file_path(instance,filename):
	ext=filename.split('.')[-1]
	filename="%s.%s" % (uuid.uuid4(),ext)
	return os.path.join(instance.directory_string_var,filename)

class images(models.Model):
	imagefile=models.ImageField(upload_to=get_file_path)
	user_id=models.ForeignKey(User)
	user_level=models.IntegerField(blank=True,null=True)
	date=models.DateField(auto_now=True)
	now=datetime.datetime.now()
	image_desc=models.CharField(max_length=200,blank=False)
	directory_string_var = 'image/%s/%s/%s/'%(now.year,now.month,now.day)
	tag_enabled=models.BooleanField()
	def __unicode__(self):
		return str(self.id)+"------"+self.image_desc

class background_images(models.Model):
	imagefile=models.ImageField(upload_to=get_file_path)
	user_id=models.IntegerField(default=1)
	user_level=models.IntegerField(default=9)
	date=models.DateField(auto_now=True)
	now=datetime.datetime.now()
	movie_id=models.ForeignKey(movies)
	directory_string_var = 'background_image/%s/%s/%s/'%(now.year,now.month,now.day)
	textcolor=models.CharField(max_length=10,default='none')
	def __unicode__(self):
		return self.movie_id.movie_name

class profile_images(models.Model):
	CHOOSEN_CHOICES = (
		(u'T',u'Choosen'),
		(u'F',u'UnChoose'),
		(u'D',u'Disable'),
		)
	imagefile=models.ImageField(upload_to=get_file_path)
	user_id=models.IntegerField(default=1)
	user_level=models.IntegerField(default=9)
	date=models.DateField(auto_now=True)
	now=datetime.datetime.now()
	person_id=models.ForeignKey(movie_persons)
	directory_string_var = 'person_image/%s/%s/%s/'%(now.year,now.month,now.day)
	image_choosen=models.CharField(max_length=2,choices=CHOOSEN_CHOICES)
	
	
	def admin_thumbnail(self):
		return u'<img src="%s" />' % (self.imagefile.url)

	admin_thumbnail.short_description = 'Thumbnail'
	admin_thumbnail.allow_tags = True

	def __unicode__(self):
		return self.person_id.person_name + " " +str(self.id)

class banner_images(models.Model):
	imagefile=models.ImageField(upload_to=get_file_path)
	user_id=models.IntegerField(default=1)
	user_level=models.IntegerField(default=9)
	date=models.DateField(auto_now=True)
	now=datetime.datetime.now()
	movie_id=models.ForeignKey(movies)
	directory_string_var = 'banner_image/%s/%s/%s/'%(now.year,now.month,now.day)

	def __unicode__(self):
		return self.movie_id.movie_name

class images_tags(models.Model):
	image_id=models.ForeignKey(images)
	TYPE_CHOICES=(
		(u'Movie',u'Movie'),
		(u'Person',u'Person'),
		(u'function',u'function'),
		(u'working-still',u'working-still'),
		)
	image_type=models.CharField(max_length=20,choices=TYPE_CHOICES,blank=True,null=True)
	person_id=models.ForeignKey(movie_persons,blank=True,null=True)
	movie_id=models.ForeignKey(movies,blank=True,null=True)	
	character_id=models.ForeignKey(movie_characters,blank=True,null=True)
	user_id=models.ForeignKey(User)
	class Meta:
		unique_together=('image_id','person_id')

	def __unicode__(self):
		output=str(self.image_id.id)+"-"
		output=output+self.image_id.image_desc+"-"
		try:
			output=output+self.person_id.person_name+"-"
		except:
			pass
		try:
			output=output+self.movie_id.movie_name+"-"
		except:
			pass
		try:
			output=output+self.character_id.character_name
		except:
			pass
		return output

class image_level(models.Model):
	image_id=models.ForeignKey(images)
	TYPE_CHOICES=(
		(u'1',u'normal'),
		(u'2',u'simple hot'),
		(u'3',u'water hot'),
		(u'4',u'simple exposed'),
		(u'5',u'hard exposed'),
		(u'6',u'working still exposed'),
		)
	images_desc=models.CharField(max_length=3,choices=TYPE_CHOICES)

class image_seq(models.Model):
	image_id=models.ForeignKey(images)
	movie_id=models.ForeignKey(movies)
	
	class Meta:
		unique_together=('image_id','movie_id')

	def __unicode__(self):
		return self.image_id.image_desc+"--"+self.movie_id.movie_name
