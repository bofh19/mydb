from genres.models import genres
from django.contrib import admin

admin.site.register(genres)