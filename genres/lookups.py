
from django.db.models import Q
from django.utils.html import escape

from ajax_select import LookupChannel

from genres.models import genres


class genresLookup(LookupChannel):

    model = genres

    def get_query(self,q,request):
        return genres.objects.filter(Q(genre_name__icontains=q)).order_by('genre_name')

    def get_result(self,obj):
        u""" result is the simple text that is the completion of what the person typed """
        return obj.genre_name

    def format_match(self,obj):
        """ (HTML) formatted item for display in the dropdown """
        return self.format_item_display(obj)

    def format_item_display(self,obj):
        """ (HTML) formatted item for displaying item in the selected deck area """
        return u"%s<hr/>" % (escape(obj.genre_name))
