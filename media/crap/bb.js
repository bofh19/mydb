
$(document).ready(function(){
ajaxCall();
   var refreshId = setInterval(function() {ajaxCall();}, 9000);});

function ajaxCall(){
$.ajax({
        type: "GET",
	url: "vals.xml",
	dataType: "xml",
	success: function(xml) {
		myHTMLOutput = '';
	 	myHTMLOutput += '<table id = "mytable" width="98%" border="1" cellpadding="0" cellspacing="0">';
	  	myHTMLOutput += '<th width="100" scope="col">Indices</th><th width="70" scope="col">Points</th><th width="70" scope="col">Pt</th><th width="80" scope="col">%</th>';
					$('company',xml).each(function(){
						var id = $(this).attr('id');
						var cname = $(this).find('cname').text();
						var points = $(this).find('cstock').text();
						var pt = ($(this).find('cstock').text() - $(this).find('pstock').text()).toFixed(2);
						var gain = ((($(this).find('cstock').text() - $(this).find('pstock').text())/$(this).find('pstock').text())*100).toFixed(2)+'%';
						
			stockData = BuildStockData(cname,points,pt,gain,id);
			myHTMLOutput = myHTMLOutput + stockData;
						});
		myHTMLOutput += '</table>';
						
$('#mytable').replaceWith(myHTMLOutput);
				
$('th').parent().addClass('table-heading');
$('tr:not([th]):odd').addClass('odd');
$('tr:not([th]):even').addClass('even');

		}
	});
}

function BuildStockData(cname,points,pt,gain,id){
	// Build HTML string and return
	output = '';
	output += '<tr>';
	output += '<TD height="45">'+ cname +'</td>';
	output += '<TD height="45">'+ points +'</td>';
	output += '<TD height="45">'+ pt +'</td>';
	output += '<TD height="45">'+ gain +'</td>';
	output += '</tr>';
	return output;
}