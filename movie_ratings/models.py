from django.db import models

# Create your models here.
from movies.models import movies

"""
NOTE CATAGORIES IN DATABASE HAS TO BE 
1 comedy
2 action
3 class

for entire rating part to work 
"""




class rating_cat(models.Model):
	cat_name=models.CharField(max_length=40,unique=True)
	cat_desc=models.CharField(max_length=100)
	def __unicode__(self):
		return self.cat_name

class movie_ratings(models.Model):
	movie_id=models.ForeignKey(movies)
	movie_rating=models.FloatField()
	movie_numofvotes=models.IntegerField()
	movie_rating_cat=models.ForeignKey(rating_cat)
	def __unicode__(self):
		return self.movie_id.movie_name

