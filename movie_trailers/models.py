from django.db import models

# Create your models here.
from movies.models import movies

class movie_trailers(models.Model):
	movie_id=models.ForeignKey(movies)
	youtube_code=models.CharField(max_length=15)
	def __unicode__(self):
		return self.movie_id.movie_name