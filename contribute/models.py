from django.db import models

# Create your models here.
from movies.models import movies
from movie_persons.models import movie_persons
from django.contrib.auth.models import User
import datetime
import uuid
import os

def get_file_path(instance,filename):
	ext=filename.split('.')[-1]
	filename="%s.%s" % (uuid.uuid4(),ext)
	return os.path.join(instance.directory_string_var,filename)

class data_text(models.Model):
	movie_id=models.ForeignKey(movies,blank=True,null=True)
	person_id=models.ForeignKey(movie_persons,blank=True,null=True)
	user_id=models.ForeignKey(User)
	text=models.CharField(max_length=5000)
	added_date = models.DateTimeField(auto_now_add=True, blank=True)
	def __unicode__(self):
		output=''
		try:
			output=output+self.movie_id.movie_name
		except Exception, e:
			pass
		try:
			output=output+self.person_id.person_name
		except Exception, e:
			pass
		return output


class data_image(models.Model):
	movie_id=models.ForeignKey(movies,blank=True,null=True)
	person_id=models.ForeignKey(movie_persons,blank=True,null=True)
	user_id=models.ForeignKey(User) 
	imagefile=models.ImageField(upload_to=get_file_path)
	now=datetime.datetime.now()
	directory_string_var = 'image/%s/%s/%s/'%(now.year,now.month,now.day)
	added_date = models.DateTimeField(auto_now_add=True, blank=True)
	def __unicode__(self):
		output=''
		try:
			output=output+self.movie_id.movie_name
		except Exception, e:
			pass
		try:
			output=output+self.person_id.person_name
		except Exception, e:
			pass
		return output
