
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse
from contribute.forms import contribute_form
from contribute.models import *
from movies.models import movies
from movie_persons.models import movie_persons
def contribute_info(request):
	status=''
	if request.user.is_authenticated():
		user=request.user
	else:
		return HttpResponse("""login before to <a href='/accounts/login/?next=/user/contribute/'>here</a>""")

	if request.method=='POST':
		form=contribute_form(request.POST,request.FILES)
		if form.is_valid():
			text=data_text()
			image=data_image()
			
			try:
				text.movie_id=movies.objects.get(id=form.cleaned_data['movie_id'])
				image.movie_id=movies.objects.get(id=form.cleaned_data['movie_id'])
			except Exception, e:
				status = status + 'No Movie'
			
			try:
				text.person_id=movie_persons.objects.get(id=form.cleaned_data['person_id'])
				image.person_id=movie_persons.objects.get(id=form.cleaned_data['person_id'])
			except Exception, e:
				status = status + 'No person'
			text.user_id=user
			image.user_id=user
			
			try:
				text.text=form.cleaned_data['text']
			except Exception, e:
				pass		

			try:
				if request.FILES['imagefile']:
					try:
						image.imagefile=request.FILES['imagefile']
						image.save()
						status = ' Image Saved. will be updated once confirmed'
					except Exception, e:
						print e
			except Exception, e:
				print e
			
			try:
				if text.text != '':
					text.save()
					status = status + ' Data Saved. will be updated once confirmed'
			except Exception, e:
				raise e
			form=contribute_form()

			return render_to_response('contribute/templates/contribute.html',
									{
									'sucess':'1',
									'status':status,
									'form':form
									},context_instance=RequestContext(request)
								)
	
		else:
			return render_to_response('contribute/templates/contribute.html',
									{
									'form':form,
									},context_instance=RequestContext(request)
								)
	
	else:
		try:
			if request.GET['person_id']:
				person_id=request.GET['person_id']
			else:
				person_id=''
		except Exception, e:
			person_id=''
		
		try:
			if request.GET['movie_id']:
				movie_id=request.GET['movie_id']
			else:
				movie_id=''
		except Exception, e:
			movie_id=''
		if movie_id=='' and person_id=='':
			form = contribute_form()
			return render_to_response('contribute/templates/contribute.html',
									{
									'form':form,
									},context_instance=RequestContext(request)
								)
		
		form = contribute_form(initial={'movie_id':movie_id,'person_id':person_id})
		return render_to_response('contribute/templates/contribute.html',
									{
									'form':form,
									},context_instance=RequestContext(request)
								)
		