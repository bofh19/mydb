from django import forms
from django.contrib.admin import widgets 
import datetime
from movie_persons.models import movie_persons
from movies.models import movies

class contribute_form(forms.Form):
	person_id=forms.IntegerField(required=False,help_text='type name and choose from drop down we will select the id for you',widget=forms.TextInput(attrs={'placeholder':'None Selected',}))
	movie_id=forms.IntegerField(required=False,help_text='type name and choose from drop down we will select the id for you',widget=forms.TextInput(attrs={'placeholder':'None Selected',}))
	text=forms.CharField(max_length=5000,widget=forms.Textarea,required=False,help_text='max 5000 chars')
	imagefile=forms.ImageField(label='select an image ',help_text='max 42 megabytes',required=False)

	def clean_person_id(self):
		person_id = self.cleaned_data['person_id']
		if person_id==None:
			return person_id
		test_person=movie_persons()
		try:
			test_person=movie_persons.objects.get(id=person_id)
			return person_id
		except test_person.DoesNotExist:
			raise forms.ValidationError(u'person with id - %s does not exists' % person_id)

	def clean_movie_id(self):
		movie_id = self.cleaned_data['movie_id']
		if movie_id==None:
			return movie_id
		test_movie=movies()
		try:
			test_movie=movies.objects.get(id=movie_id)
			return movie_id
		except test_movie.DoesNotExist:
			raise forms.ValidationError(u'movie with id- %s does not exists' % movie_id)

	def clean(self):
		cleaned_data = super(contribute_form, self).clean()
		person_id = cleaned_data.get('person_id')
		movie_id = cleaned_data.get('movie_id')
		
		if person_id==None and movie_id==None:
			raise forms.ValidationError(u'Have to Choose Yetleast one from person and movie')
		else:
			return cleaned_data

