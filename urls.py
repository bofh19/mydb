# from django.conf.urls.defaults import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls.defaults import *
from ajax_select import urls as ajax_select_urls
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'mydb.views.home', name='home'),
    # url(r'^mydb/', include('mydb.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^parse/',include('wiki_parser.urls')),
    url(r'^music/',include('movie_music.urls')),
    url(r'^ajx/',include('ajx.urls')),
    url(r'^v2/',include('indexv2.urls')),
    url(r'^v1/',include('indexv1.urls')),
    url(r'^mobile/v1/',include('mobile_api_v1.urls')),

    url(r'^admin/lookups/', include(ajax_select_urls)),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('registration.backends.default.urls')),
  #  url(r'^movie/(?P<movie_id>\d+)/$','movies.views.movie_detail'),
  #http://127.0.0.1:8000/movie/15-None/
   url(r'^movie/15-(.)?','index.views.movie_default_page'),
   # url(r'^footer/$','index.views.footer',name='footer'),
   # url(r'^footer/2/$','index.views.footer2',name='footer2'),
   # url(r'^getanotherimage/$','index.views.diff_back_img',name='diff_back_img'),
)
urlpatterns += patterns('contribute.views',
     url(r'^user/contribute/$','contribute_info'),
)

urlpatterns += patterns('movies.views',

    # url(r'^movie/all/$','movie_list_all'),
    url(r'^movie/search/$','movie_search'),
    url(r'^movie/searching/', 'movie_searching'),
    # url(r'^add/movie/entermovie/', 'movie_enter',name='movie_enter'), #verifying status 
    # url(r'^add/movie/enteringmovie/', 'movie_entering'), #verifying
    ###############################
    # url(r'^movie/(?P<movie_id>\d+)\-?([^/]?)+/$','movie_detail_plot',name='movie_detail_plot'),
    # url(r'^movie/(?P<movie_id>\d+)\-?([^/]?)+/rating/$','movie_detail_rating',name='movie_detail_rating'),
    # url(r'^movie/(?P<movie_id>\d+)\-?([^/]?)+/cast/$','movie_detail_cast'),
    # url(r'^movie/(?P<movie_id>\d+)\-?([^/]?)+/music/$','movie_detail_music'),
    # url(r'^movie/(?P<movie_id>\d+)\-?([^/]?)+/trailers/$','movie_detail_trailers'),

    # url(r'^movie/(?P<movie_id>\d+)\-?([^/]?)+/index/$','movie_detail_index',name='movie_detail_index'),
    ###############################
	)

urlpatterns += patterns('movie_news.views',
        # url(r'^movie/(?P<movie_id>\d+)\-?([^/]?)+/news/$','movie_detail_news',name='movie_detail_news'),
        # url(r'^news/(?P<news_id>\d+)/$','news_detail',name='news_detail'),
    )

urlpatterns += patterns('movie_music.views',
            # url(r'^add/movie_music/ajaxaddurl/$', 'ajaxaddurl'), #anyone can add this for now
            )


urlpatterns += patterns('user_votes.views',
            # url(r'^movie/(?P<movie_id>\d+)/ajaxrating/', 'ajaxrate'),
            )

urlpatterns += patterns('user_profile.views',
   # url(r'^user/register/$','user_register'),
    #url(r'^user/registering/$','user_registering'),
    #url(r'^user/login/$','user_login'),
   # url(r'^user/loging/$','user_loging'),
  #  url(r'^user/logout/$','user_logout',name='logout'),    
   # url(r'^user/pagex/$','page_x'),
)

urlpatterns += patterns('images.views',
     # url(r'^image/background/all/$','back_images_all'),
     # url(r'^image/background/(?P<image_id>\d+)/$','back_image'),
    # url(r'^image/list/$','listview',name='listview'),
    # url(r'^image/listurl/$','listview_url',name='listview_url'),
    # url(r'^image/(?P<image_id>\d+)/$','image_view'),
    # url(r'^image/(?P<image_id>\d+)/ajaxtaging/$','image_tag'),
    # url(r'^image/(?P<movie_id>\d+)\-?([^/]?)+/(?P<image_id>\d+)/$','image_movie_view'),
    # url(r'^image/(?P<movie_id>\d+)\-?([^/]?)+/$','image_movie_view_all'),
    # url(r'^person/image/(?P<person_id>\d+)\-?(.?)+/(?P<image_id>\d+)/$','person_detail_image'),
    # url(r'^person/image/(?P<person_id>\d+)\-?(.?)+/$','person_detail_images'),
    # url(r'^profile/image/(?P<person_id>\d+)\-?(.?)+/(?P<image_id>\d+)/$','profile_detail_image'),
    # url(r'^image/background/(?P<movie_id>\d+)\-?([^/]?)+/(?P<image_id>\d+)/$','back_image_movie_view'),
    # url(r'^image/background/all/(?P<movie_id>\d+)\-?([^/]?)+/$','back_image_movie_view_all'),
    )

urlpatterns += patterns('movie_casting.views',
    # url(r'^add/cast/addcast/(?P<movie_id>\d+)\-?([^/]?)+/$','addcast'), #verifying
    # url(r'^add/cast/addingcast/(?P<movie_id>\d+)\-?([^/]?)+/$','addingcast'), #verifying
    )

urlpatterns += patterns('professions.views',
    url(r'^profession/searching/', 'profession_searching'),
    # url(r'^add/profession/addprof/$', 'add_prof'), #verifying
    )

urlpatterns += patterns('index.views',
    #url(r'^$','main_page',name="index"),
    url(r'^main/$','main_page',name="index_main"), #home button uses this
    url(r'^$','releasing',name="index"),
    # url(r'^add/$', 'add_page'),
    # url(r'^list/$', 'list_page'),
    # url(r'^movies/latest/theaters/$','latest_theaters',name="latest_theaters"),
    url(r'^movies/latest/trailers/$','latest_trailers',name="latest_trailers"),
    # url(r'^movies/latest/db/$','latest_db',name="latest_db"),
    # url(r'^movies/latest/releasing/$','releasing',name="releasing"),
    url(r'^movies/latest/news/$','latest_news',name="latest_news"),
    # url(r'^all/searching/$','all_searching',name="all_searching"), #ajax call
    # url(r'^search/all/$','search_all',name="search_all"),
    )

urlpatterns += patterns('movie_persons.views',
    # url(r'^person/(?P<person_id>\d+)\-?(.?)+/info/$','person_detail_info',name="person_detail_info"),
    # url(r'^person/(?P<person_id>\d+)\-?(.?)+/movies/$','person_detail_movies'),
    #url(r'^person/(?P<person_id>\d+)\-?(.?)+/music/$','person_detail_music'),
    #url(r'^person/(?P<person_id>\d+)\-?(.?)+/$','person_detail_info'),
    # url(r'^person/(?P<person_id>\d+)\-?(.?)+/$','person_detail_index'),
    # url(r'^add/person/addperson/$','enter_person'), #verifying
    # url(r'^add/person/(?P<person_id>\d+)\-?(.?)+/$','edit_person',name='edit_person_url'), #verifying
    
    # url(r'^person/(?P<person_id>\d+)\-?(.?)+/index/$','person_detail_index',name='person_detail_index'),
    # url(r'^person/(?P<person_id>\d+)\-?(.?)+/trailers/$','person_detail_trailers',name='person_detail_trailers'),
    url(r'^person/searching/', 'person_searching'),
    # url(r'^person/all/$', 'movie_persons_listall',name='movie_persons_listall'),
    )

urlpatterns += patterns('user_profile.views',
        url(r'^user/profile/$','user_profile_main',name="user_profile_main"),
       # url(r'^user/profile/redirect/$','redirect_page',name="redirect_page"),
        )
urlpatterns += patterns('',
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout',name="user_logout"),
    url(r'', include('social_auth.urls')),
    )+static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)

