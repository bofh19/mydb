"""
movie name
release / released date
background image
plot

give colums for hero heroine's director

cast in movie ---- > enter cast on pop up window if they dont exist


"""
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from movies.forms import *
from movies.models import movies
from django.core.urlresolvers import reverse

from images.models import banner_images
from images.models import background_images
def movie_enter(request):
	#verifying is staff status here
	if request.user.is_authenticated():
		if request.user.is_staff:
			username=request.user.username
		else:
			return HttpResponse("user not staff click back on browser to go back")
	else:
		return HttpResponse("""login before to <a href=/user/login/>login here</a>""")
		
	p=enter_movie_form()
	return render_to_response('movies/templates/movie_enter.html',{'form':p},context_instance=RequestContext(request))

def movie_entering(request):

	f=enter_movie_form(request.POST)
	if f.is_valid():
		pass
	else:
		return HttpResponse("form data not valid")
	if request.method=='POST':
		#verifying is staff status here
		if request.user.is_authenticated():
			if request.user.is_staff:
				username=request.user.username
			else:
				return HttpResponse("user not staff click back on browser to go back")
		else:
			return HttpResponse("""login before to <a href=/user/login/>login here</a>""")
		new_movie=movies()
		new_movie_name=f.cleaned_data['movie_name']
		try:
			mo=movies.objects.get(movie_name__iexact=new_movie_name)
			return HttpResponse("movie with that name already exists")
		except:
			pass
		new_movie.movie_name=new_movie_name.title()
		new_movie.movie_year=f.cleaned_data['movie_year']
		new_movie.movie_plot=f.cleaned_data['movie_plot']
		new_movie.movie_summary=f.cleaned_data['movie_summary']
		new_movie.save()
		try:

			if request.FILES['movie_banner_image']:
				newbanner=banner_images(imagefile=request.FILES['movie_banner_image'])
				newbanner.user_id=request.user.id
				newbanner.user_level=9
				newbanner.movie_id=new_movie
				try:
					newbanner.save()
				except:
					new_movie.delete()
					return HttpResponse("error saving banner did not save movie either go back")
		except Exception,e:
			print e
		try:
			if request.FILES['movie_background_image']:
				newbackground=background_images(imagefile=request.FILES['movie_background_image'])
				newbackground.user_id=request.user.id
				newbackground.user_level=9
				newbackground.movie_id=new_movie
				try:
					newbackground.save()
				except:
					pass
				else:
					return HttpResponse("ERROR 2")
		except:
			pass
		movie_id=new_movie.id
		movie_name="-"+new_movie.movie_name
		return HttpResponseRedirect(reverse('movie_casting.views.addcast',  args=(movie_id,movie_name)))
			
