from django.shortcuts import render_to_response
from django.shortcuts import get_object_or_404
from movies.models import movies
from django.template import RequestContext
from movie_genres.models import movie_genres
from genres.models import genres
from user_votes.models import user_votes
from movie_ratings.models import movie_ratings
from images.models import image_seq
from images.models import images
from images.models import profile_images
from movie_casting.models import movie_casting
from movie_persons.models import movie_persons
from movie_characters.models import movie_characters
from images.models import background_images
from images.models import banner_images
from movie_ratings.models import rating_cat
from movie_music.models import movie_music, song_persons_info, song_video_info
from movies.models import movie_extra_info
from movie_music.models import song_links
from professions.models import professions
import json, random
from movies.models import movie_theater_status
from index import global_var
from wiki_parser.models import movie_wlinks

MIN_NUM_VOTES = 1
MEDIA_SERVER_URL="http://127.0.0.1:8000"
def sortpowh(a):
	x=len(a)
	i=0
	#print x
	while(x>1):
		#print x
		i=0
		while(i<(x-1)):
			if a[i]['powh']>a[i+1]['powh']:
				b=a[i]
				a[i]=a[i+1]
				a[i+1]=b
			i=i+1
		x=x-1
	a.reverse()
	return a
def getpowh70(a):
	z=[]
	for x in a:
		if x['powh']>=70:
			z.append(x)
	return z	
		
from datetime import *
def movie_detail_index(request,movie_id):
	if request.user.is_authenticated():
		last_login=request.session.get('social_auth_last_login_backend')
	else:
		last_login=''

	try:
		movie_name=get_object_or_404(movies,id=movie_id)
	except UnboundLocalError:
		movie_name="Does Not Exist"
		return render_to_response('movies/templates/movie_detail_index.html',
								{
									'name':movie_name,
								},context_instance=RequestContext(request)
							)
	except movies.DoesNotExist:
		movie_name="Does Not Exist"
		return render_to_response('movies/templates/movie_detail_index.html',
								{
									'name':movie_name,
								},context_instance=RequestContext(request)
						)
####################### Extra Info
	try:
		extra_infos=movie_extra_info.objects.filter(movie_id=movie_id)
		
	except Exception, e:
		extra_infos=''


	try:
		
		this_movie_backimages=background_images.objects.filter(movie_id=movie_id)
		this_movie_backimage=random.choice(this_movie_backimages)
	except background_images.DoesNotExist:
		this_movie_backimages=[]
		this_movie_backimage=''
	except IndexError:
		this_movie_backimages=[]
		this_movie_backimage=''
	except Exception,e:
		this_movie_backimages=[]
		this_movie_backimage=''
	
	if not this_movie_backimages:
		plot_response=plot_function(request,movie_id)
		return render_to_response('movies/templates/movie_detail_plot.html',plot_response,context_instance=RequestContext(request))
	
	movie=movies.objects.get(id=movie_id)

	movie_json={}
	movie_json['id']=movie

	try:
		genres=movie_genres.objects.filter(movie_id=movie.id)
	except UnboundLocalError:
		genres="Does Not Exist"
	except movie_genres.DoesNotExist:
		genres="Does Not Exist"

	movie_json['genres']=genres

	try:
		movie_per=[]
		movie_pers=[]
		try:
			prof_id=professions.objects.get(profession_name='Hero')
		except Exception, e:
			pass
		try:
			this_movie_persons=movie_casting.objects.filter(movie_id=movie.id,profession_id=prof_id.id)
			#print this_movie_persons
		except Exception, e:
			pass
		for person in this_movie_persons:
			movie_per.append(movie_persons.objects.get(id=person.movie_persons_id.id))

		prof_id=professions.objects.get(profession_name='Heroine')
		this_movie_persons=(movie_casting.objects.filter(movie_id=movie.id,profession_id=prof_id.id))
		for person in this_movie_persons:
			movie_per.append(movie_persons.objects.get(id=person.movie_persons_id.id))

		prof_id=professions.objects.get(profession_name='Director')
		this_movie_persons=(movie_casting.objects.filter(movie_id=movie.id,profession_id=prof_id.id))
		for person in this_movie_persons:
			movie_per.append(movie_persons.objects.get(id=person.movie_persons_id.id))

		prof_id=professions.objects.get(profession_name='Music Director')
		this_movie_persons=(movie_casting.objects.filter(movie_id=movie.id,profession_id=prof_id.id))
		for person in this_movie_persons:
			movie_per.append(movie_persons.objects.get(id=person.movie_persons_id.id))

		prof_id=professions.objects.get(profession_name='Producer')
		this_movie_persons=(movie_casting.objects.filter(movie_id=movie.id,profession_id=prof_id.id))
		 		
		for person in this_movie_persons:
			movie_per.append(movie_persons.objects.get(id=person.movie_persons_id.id))

		movie_per=list(set(movie_per))

		for person in movie_per:
			person_json={}
			person_json['id']=person
			try:
				person_json['profile_image']=random.choice(profile_images.objects.filter(person_id=person.id,image_choosen='T'))
			except Exception, e:
				person_json['profile_image']=''
			movie_pers.append(person_json)
		movie_json['persons']=movie_pers
	except Exception, e:

		movie_json['persons']=''

	try:
		this_movie_banner_images=banner_images.objects.filter(movie_id=movie_id)
		this_movie_banner_img=random.choice(this_movie_banner_images)
		this_movie_banner_image=this_movie_banner_img.imagefile.url
	except banner_images.DoesNotExist:
		this_movie_banner_image=''
	except:
		this_movie_banner_image=''

	try:
		this_movie_theater_status=movie_theater_status.objects.get(movie_id=movie_id).release_status
	except Exception, e:
		this_movie_theater_status=''
	
	now=date.today()
	diff=movie.movie_year-now
	days_left=diff.days
	return render_to_response('movies/templates/movie_detail_index.html',
								{
									#'background_images':this_movie_backimages,
									'movie_id':movie_id,
									'name':movie_name,
									'extra_infos':extra_infos,
									'last_login':last_login,
									'background_image':this_movie_backimage,
									'movie':movie_json,
									'banner_image':this_movie_banner_image,
									'theater_status':this_movie_theater_status,
									'global_var':global_var,
									'days_left':days_left,
								},context_instance=RequestContext(request)
							)

def plot_function(request,movie_id):
	if request.user.is_authenticated():
		last_login=request.session.get('social_auth_last_login_backend')
	else:
		last_login=''
	try:
		this_movie_theater_status=movie_theater_status.objects.get(movie_id=movie_id).release_status
	except Exception, e:
		this_movie_theater_status=''
	try:
		movie_name=get_object_or_404(movies,id=movie_id)
		movie_year=(movie_name.movie_year).year
		movie_plot=movie_name.movie_plot
		movie_summary=movie_name.movie_summary
	except UnboundLocalError:
		movie_name="Does Not Exist"
		plot_response=	{
									'name':movie_name,
						}
		return plot_response
	except movies.DoesNotExist:
		movie_name="Does Not Exist"
		plot_response={
									'name':movie_name,
								}
		return plot_response
####################### Extra Info
	try:
		extra_infos=movie_extra_info.objects.filter(movie_id=movie_id)
		

	except Exception, e:
		extra_infos=''
##################################	Genres
	try:
		genre=movie_genres.objects.filter(movie_id=movie_id)
		genres=[]
		for gen in genre:
			a=gen.genre_id
			genres.append(a)
	except UnboundLocalError:
		genres="Does Not Exist"
	except movie_genres.DoesNotExist:
		genres="Does Not Exist"

####################################### BANNER IMAGE
	try:
		this_movie_banner_images=banner_images.objects.filter(movie_id=movie_id)
		this_movie_banner_img=random.choice(this_movie_banner_images)
		this_movie_banner_image=this_movie_banner_img.imagefile.url
	except banner_images.DoesNotExist:
		this_movie_banner_image=''
	except:
		this_movie_banner_image=''

	now=date.today()
	diff=movie_name.movie_year-now
	days_left=diff.days
	#print days_left
	wiki_link=''
	try:
		wiki_link=movie_wlinks.objects.get(movie_id=movie_id)
	except Exception, e:
		wiki_link=''
	plot_response={
									'movie_id':movie_id,
									'name':movie_name,
									'plot':movie_plot,
									'movie_year':movie_year,
									'genres':genres,
									'banner_image':this_movie_banner_image,
									'extra_infos':extra_infos,
									'summary':movie_summary,
									'last_login':last_login,
									'theater_status':this_movie_theater_status,
									'days_left':days_left,
									'wiki_link':wiki_link,
								}

	return plot_response

def movie_detail_plot(request,movie_id):
	plot_response=plot_function(request,movie_id)
	return render_to_response('movies/templates/movie_detail_plot.html',plot_response,context_instance=RequestContext(request))

def movie_detail_rating(request,movie_id):
	try:
		movie_name=get_object_or_404(movies,id=movie_id)
		movie_year=(movie_name.movie_year).year
		movie_plot=movie_name.movie_plot
	except UnboundLocalError:
		movie_name="Does Not Exist"
		return render_to_response('movies/templates/movie_detail_plot.html',
								{
									'name':movie_name,
								},context_instance=RequestContext(request)
							)
	except movies.DoesNotExist:
		movie_name="Does Not Exist"
		return render_to_response('movies/templates/movie_detail_plot.html',
								{
									'name':movie_name,
								},context_instance=RequestContext(request)
						)

##############################  Rating 
	"""
	   data one : comedy.numofones, action.numofones, class.noofones
	   data two : 
	   data three: 
	"""
	try:
		comedy_cat=rating_cat.objects.get(cat_name='comedy')
		action_cat=rating_cat.objects.get(cat_name='action')
		class_cat=rating_cat.objects.get(cat_name='class')
		dataone = [len(user_votes.objects.filter(movie_id=movie_id,rating_cat=comedy_cat,rating=1)),
				len(user_votes.objects.filter(movie_id=movie_id,rating_cat=action_cat,rating=1)),
				len(user_votes.objects.filter(movie_id=movie_id,rating_cat=class_cat,rating=1))
					]

		datatwo = [len(user_votes.objects.filter(movie_id=movie_id,rating_cat=comedy_cat,rating=2)),
				len(user_votes.objects.filter(movie_id=movie_id,rating_cat=action_cat,rating=2)),
				len(user_votes.objects.filter(movie_id=movie_id,rating_cat=class_cat,rating=2))
					]
		
		datathree = [len(user_votes.objects.filter(movie_id=movie_id,rating_cat=comedy_cat,rating=3)),
				len(user_votes.objects.filter(movie_id=movie_id,rating_cat=action_cat,rating=3)),
				len(user_votes.objects.filter(movie_id=movie_id,rating_cat=class_cat,rating=3))]
	except:
		dataone=[]
		datatwo=[]
		datathree=[]
	
	return render_to_response('movies/templates/movie_detail_rating.html',
								{
									'movie_id':movie_id,
									'name':movie_name,
									'plot':movie_plot,
									'movie_year':movie_year,
									'dataone':dataone,      # rating data
									'datatwo': datatwo,     # rating data
									'datathree':datathree,  # ratind data
									
								},context_instance=RequestContext(request)
							)

def movie_detail_cast(request,movie_id):

	try:
		movie_name=get_object_or_404(movies,id=movie_id)
		movie_year=(movie_name.movie_year).year
		movie_plot=movie_name.movie_plot
	except UnboundLocalError:
		movie_name="Does Not Exist"
		return render_to_response('movies/templates/movie_detail_plot.html',
								{
									'name':movie_name,
								},context_instance=RequestContext(request)
							)
	except movies.DoesNotExist:
		movie_name="Does Not Exist"
		return render_to_response('movies/templates/movie_detail_plot.html',
								{
									'name':movie_name,
								},context_instance=RequestContext(request)
						)

##################### persons
	try:
		this_movie_persons=movie_casting.objects.filter(movie_id=movie_id)
		
		persons_id=[]
		#print "x"
		for this_movie_person in this_movie_persons:
			#print this_movie_person.movie_persons_id.id
			persons_id.append(this_movie_person.movie_persons_id.id)		
		persons_id=list(set(persons_id))
	except movie_casting.DoesNotExist:
		pass
	result=[]
	#print persons_id
	
	for person_id in persons_id:
	#	print person_id
		person_json={}
		profession_json=[]
		person_json['id']=person_id
		person_json['powh']=0
		try:
			person_json['name']=movie_persons.objects.get(id=person_id).person_name
			#print person_json['name']
		except movie_persons.DoesNotExist:
			person_json['name']="Not avilable error"+str(person_id)
		try:

			person_json['profile_image']=random.choice(profile_images.objects.filter(person_id=person_id,image_choosen='T'))
		except:
			person_json['profile_image']=''
		try:
			person_json['character'] = movie_characters.objects.get(movie_persons_id=person_id,movie_id=movie_id).character_name			
		except:
			person_json['character']=''	
		try:
			person_json['character_id'] = movie_characters.objects.get(movie_persons_id=person_id,movie_id=movie_id).id
		except:
			person_json['character_id']=''
		for z in movie_casting.objects.filter(movie_persons_id=person_id,movie_id=movie_id):
			person_professions={}
			person_professions['id']=z.profession_id.id
			person_professions['name']=z.profession_id.profession_name
			person_json['powh']=person_json['powh']+z.profession_id.profession_level
			profession_json.append((person_professions))
		person_json['professions']=profession_json
		result.append(person_json)
	#asdf=json.dumps(result)
	#print asdf
	result=sortpowh(result)
	persons=result

	return render_to_response('movies/templates/movie_detail_cast.html',
								{
									'movie_id':movie_id,
									'name':movie_name,
									'plot':movie_plot,
									'movie_year':movie_year,
									'persons':persons,
								},context_instance=RequestContext(request)
							)

def movie_detail_music(request,movie_id):
	try:
		movie_name=get_object_or_404(movies,id=movie_id)
		movie_year=(movie_name.movie_year).year
		movie_plot=movie_name.movie_plot
		movie_summary=movie_name.movie_summary
	except UnboundLocalError:
		movie_name="Does Not Exist"
		return render_to_response('movies/templates/movie_detail_music.html',
								{
									'name':movie_name,
								},context_instance=RequestContext(request)
							)
	except movies.DoesNotExist:
		movie_name="Does Not Exist"
		return render_to_response('movies/templates/movie_detail_music.html',
								{
									'name':movie_name,
								},context_instance=RequestContext(request)
						)
	try:
		this_movie_muzic=movie_music.objects.filter(movie_id=movie_id)
	except Exception, e:
		this_movie_muzic=''
	this_movie_music_persons=[]
	this_movie_music_video=[]

	this_movie_music=[]
	for song in this_movie_muzic:
		this_movie_music_json={}
		this_movie_music_json['song']=song
		this_song_persons_info=[]
		this_song_video_info=[]
		this_song_urls=[]
		try:
			this_song_persons_all=song_persons_info.objects.filter(song_id=song.id)
		except Exception, e:
			this_song_persons_all=[]
		
		for this_song_person in this_song_persons_all:
			person_info_json={}
			person_info_json['id']=this_song_person.person_id
			person_info_json['desc']=this_song_person.person_desc
			this_song_persons_info.append(person_info_json)
		this_movie_music_persons.append(this_song_persons_info)
		
		try:
			this_song_video_all=song_video_info.objects.filter(song_id=song.id)
		except Exception, e:
			this_song_video_all=[]
		for this_song_person in this_song_video_all:
			person_info_json={}
			person_info_json['id']=this_song_person.person_id
			person_info_json['desc']=this_song_person.person_desc
			this_song_video_info.append(person_info_json)
		
		try:
			this_song_urls_all=song_links.objects.filter(song_id=song.id)
		except Exception, e:
			this_song_urls_all=[]
		for this_song_url in this_song_urls_all:
			this_song_urls.append(this_song_url)

		this_movie_music_json['video_persons']=this_song_video_info
		this_movie_music_json['persons']=this_song_persons_info
		this_movie_music_json['urls']=this_song_urls
		this_movie_music.append(this_movie_music_json)
	
	return render_to_response('movies/templates/movie_detail_music.html',
								{
									'name':movie_name,
									'movie_id':movie_id,
									'movie_music':this_movie_music,
								},context_instance=RequestContext(request)
						)

from movie_trailers.models import movie_trailers

def movie_detail_trailers(request,movie_id):
	try:
		movie_name=movies.objects.get(id=movie_id).movie_name
	except Exception, e:
		movie_name=''

	if request.user.is_authenticated():
		last_login = request.session.get('social_auth_last_login_backend')
	else:
		last_login=''
	
	this_trailers=movie_trailers.objects.filter(movie_id=movie_id)[::-1]
	latest_trailers=this_trailers
	
	return render_to_response('movies/templates/movie_detail_trailers.html',
								{
								'latest_trailers':latest_trailers,
								'movie_id':movie_id,
								'name':movie_name,
								'last_login':last_login
								},
								context_instance=RequestContext(request)
								)