from django.shortcuts import render_to_response
from django.template import RequestContext

from movies.models import movies
from movie_ratings.models import movie_ratings
#import json
from images.models import banner_images
import random
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

def movie_list_all(request):
	items_per_page=30
	if request.user.is_authenticated():
		last_login = request.session.get('social_auth_last_login_backend')
	else:
		last_login=''
	
	try:
		if request.GET['p']:
			page_number=int(request.GET['p'])
		else:
			page_number=1
	except Exception, e:
		page_number=1

	page=page_number

	try:
		movies_list=movies.objects.all()[::-1]
		results=[]
	except movies.DoesNotExist:
		movies_list=''
	
	paginator = Paginator(movies_list,items_per_page)

	try:
		all_movies = paginator.page(page)
	except PageNotAnInteger:
		all_movies = paginator.page(1)
	except EmptyPage:
		all_movies = paginator.page(paginator.num_pages)

	for movie in all_movies:
		movie_json={}
		movie_json['name']=movie.movie_name
		movie_json['id']=movie.id
		try:
			this_movie_banner_images=banner_images.objects.filter(movie_id=movie.id)
			this_movie_banner_img=random.choice(this_movie_banner_images)
			movie_json['banner_image']=this_movie_banner_img
		except banner_images.DoesNotExist:
			movie_json['banner_image']=''
		except:
			movie_json['banner_image']=''

		try:
			movie_json['rating']=movie_ratings.objects.get(movie_id=movie.id).movie_rating
		except:
			movie_json['rating']="DoesNotExist"
		movie_json['year']=movie.movie_year.year
		results.append(movie_json)
	#movie_list=json.dumps(results)
	movie_list=results
	#print movie_list
	if request.user.is_authenticated():
		username=request.user.username
	else:
		username=""
	return render_to_response('movies/templates/movie_list_all.html',
								{
								'movies_list':movie_list,
								  'username':username,
								  'last_login':last_login,
								'all_movies':all_movies,
								
								},								
								context_instance=RequestContext(request)
							)

