from django.conf.urls import patterns, include, url

urlpatterns = patterns('wiki_parser.views',
    url(r'^movie/cast/(?P<movie_id>\d+)/$','movie_parse_cast',name='movie_parse_cast'),
    url(r'^movie/fields/(?P<movie_id>\d+)/$','movie_parse_fields',name='movie_parse_fields'),
    url(r'^add/person/$','person_parser_add',name='person_parser_add'),
    url(r'^add/movie/$','movie_parser_add',name='movie_parser_add'),

    #ajax
    url(r'^movie/cast/ajaxlinking/', 'parser_ajaxlink'),

    url(r'^add/person/adding/$', 'person_parser_adding',name='person_parser_adding'),

    url(r'^add/movie/adding/$','movie_parser_adding',name='movie_parser_adding')
)
