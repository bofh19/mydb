# Create your views here.

from django.http import HttpResponse
import urllib2
import json
import re
from wiki_parser.models import movie_wlinks
from wiki_parser.models import person_wlinks
from django.contrib.auth.models import *

from django.shortcuts import render_to_response
from django.template import RequestContext
from movie_persons.models import movie_persons
from movies.models import movies
import random


from django.db.models import Q
def movie_parse_cast(request,movie_id):
	if request.user.is_staff:
		pass
	else:
		output = "user not staff"
		return render_to_response('wiki_parser/templates/parse_cast.html',
								{
									'output':output,
								},context_instance=RequestContext(request)
							)
	try:
		wtitle=movie_wlinks.objects.get(movie_id=movie_id)
		movie_id=movies.objects.get(id=movie_id)
	except Exception, e:
		output = 'wtitle does not exists for this movie add in admin'
		return render_to_response('wiki_parser/templates/parse_cast.html',
								{
									'output':output,
								},context_instance=RequestContext(request)
							)


	output2 = []
	urlx="""http://en.wikipedia.org/w/api.php?action=query&prop=links&titles="""+str(wtitle.wtitle)+"""&pllimit=5000&format=json"""
	urlx = re.sub(' ','_',urlx)
	f = urllib2.urlopen(urlx)
	a=json.loads(f.read())
	links =  a['query']['pages'][a['query']['pages'].keys()[0]]['links']
	output=''
	index=0
	for link in links:
		outputx = {}
		outputx['matches']=[]
		output = output+link['title']
		outputx['link']=link['title']
		checkone_str=link['title']
		checkone_str = re.sub(' ','_',checkone_str)

		try:
			checkone_res = person_wlinks.objects.filter(wtitle__iexact=checkone_str)
		except Exception, e:
			pass
		if checkone_res:
			matchesz=[]
			for checkone_res_each in checkone_res:
				matchesz.append(movie_persons.objects.get(id=checkone_res_each.person_id.id))
			outputx['matches']=matchesz
		else:
			query_obj= re.split(r' ',link['title'])
			for query_obj_item in query_obj:
				if len(query_obj_item) > 2 and query_obj_item != '(actor)':
					try:
						a=movie_persons.objects.filter(Q(person_name__icontains=query_obj_item))
					except Exception, e:
						pass
					if a!=[]:
						
						for item in a:
							output = output+"----"+"<a href='/person/"+(str(item.id))+"/index/'>"+item.person_name+"</a>"
							outputx['matches'].append(item)
					else:
						output = output + "none"
				outputx['matches']=list(set(outputx['matches']))
			output = output+'\n<br/>'

		output2.append(outputx)
	output = output+'<hr/>'
		
	return render_to_response('wiki_parser/templates/parse_cast.html',
								{
									'output':output,
									'results':output2,
									'movie':movie_id,
									'movie_wlink':str(wtitle.wtitle),
								},context_instance=RequestContext(request)
							)
import json
from movie_casting.models import movie_casting
from professions.models import professions

def parser_ajaxlink(request):
	ajaxresponse = {}

	if request.user.is_staff: 							
		pass
	else:
		ajaxresponse['message'] = 'user not staff to do this'
		ajaxresponse = json.dumps(ajaxresponse)
		return HttpResponse(ajaxresponse)
	
	try:
		person_id=request.GET['person_id']
		movie_id = request.GET['movie_id']
#new_person_profession=professions.objects.get(profession_desc__iexact=f.cleaned_data['person_profession'])
		movie_name = movies.objects.get(id=movie_id)
		person_name = movie_persons.objects.get(id=person_id)
	except Exception, e:
		ajaxresponse['message'] = 'ERROR'
		ajaxresponse = json.dumps(ajaxresponse)
		return HttpResponse(ajaxresponse)
	
	try:
		res=movie_casting.objects.filter(movie_id=movie_id,movie_persons_id=person_id)
		if res:
			ajaxresponse['message'] = '<strong>'+movie_name.movie_name+'</strong> and <strong>'+person_name.person_name+'</strong> are already linked'
			ajaxresponse = json.dumps(ajaxresponse)
			return HttpResponse(ajaxresponse)
	except Exception, e:
		pass

	new_cast = movie_casting()
	
	new_cast.movie_persons_id=person_name
	new_cast.movie_id=movie_name
	profession_given='0'
	try:
		prof = request.GET['prof']
		prof_id=professions.objects.get(profession_desc__iexact=prof)
		profession_given='1'
	except Exception, e:
		try:
			prof_id=professions.objects.filter(profession_name='unknown')[0]
		except Exception, e:
			return HttpResponse('error near professions sorry contact coder :P')
	
	new_cast.profession_id=prof_id
	try:
		new_cast.save()
	except Exception, e:
		ajaxresponse['message'] = 'unable to save probably already exists'
		ajaxresponse = json.dumps(ajaxresponse)
		return HttpResponse(ajaxresponse)

	ajaxresponse['person_id'] = person_id
	ajaxresponse['movie_id'] = movie_id

	ajaxresponse['message'] = '<strong>'+movie_name.movie_name+'</strong> and <strong>'+person_name.person_name+'</strong> with profession <strong>'+prof_id.profession_name+'('+str(prof_id.profession_level)+')'+'</strong> linked - profession_given'+str(profession_given)

	#print ajaxresponse['message']
	ajaxresponse = json.dumps(ajaxresponse)
	return HttpResponse(ajaxresponse)

def movie_parse_fields(request,movie_id):
	if request.user.is_staff:
		pass
	else:
		output = "user not staff"
		return render_to_response('wiki_parser/templates/parse_fields.html',
								{
									'output':output,
								},context_instance=RequestContext(request)
							)
	try:
		wtitle=movie_wlinks.objects.get(movie_id=movie_id)
	except Exception, e:
		output = 'wtitle does not exists for this movie add in admin'
		return render_to_response('wiki_parser/templates/parse_fields.html',
								{
									'output':output,
								},context_instance=RequestContext(request)
							)
	output =''
	urlx="""http://en.wikipedia.org/w/api.php?action=query&prop=revisions&rvprop=content&format=json&titles="""+str(wtitle.wtitle)+"""&rvsection=0&pllimit=5000"""
	urlx = re.sub(' ','_',urlx)
	f = urllib2.urlopen(urlx)
	a=json.loads(f.read())	
	infobox = a['query']['pages'][a['query']['pages'].keys()[0]]['revisions']
	info = re.split(r'\\n\|',str(infobox[0]))
	index=0
	while(index<len(info)):
		output = output+str(info[index])+'<br/>'
		index=index+1
	output = output+'<hr/>'
	return render_to_response('wiki_parser/templates/parse_fields.html',
								{
									'output':output
								},context_instance=RequestContext(request)
							  )


