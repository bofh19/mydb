
from wiki_parser.models import *
from django.contrib import admin

from ajax_select import make_ajax_form
from ajax_select.admin import AjaxSelectAdmin

class movie_wlinksAdmin(AjaxSelectAdmin):
	model = movie_wlinks
	form = make_ajax_form(movie_wlinks,{'movie_id':'movies_lookups'})

class person_wlinksAdmin(AjaxSelectAdmin):
	model = person_wlinks
	form=make_ajax_form(person_wlinks,{'person_id':'movie_person_lookups'})

admin.site.register(movie_wlinks,movie_wlinksAdmin)
admin.site.register(person_wlinks,person_wlinksAdmin)