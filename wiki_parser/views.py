# Create your views here.

from django.http import HttpResponse
import urllib2
import json
import re
from wiki_parser.models import movie_wlinks
from django.contrib.auth.models import *

from django.shortcuts import render_to_response
from django.template import RequestContext
from movie_persons.models import movie_persons
from movies.models import movies
import random

from django.db.models import Q
def movie_parse_cast(request,movie_id):
	if request.user.is_staff:
		pass
	else:
		output = "user not staff"
		return render_to_response('wiki_parser/templates/parse_cast.html',
								{
									'output':output,
								},context_instance=RequestContext(request)
							)
	try:
		wtitle=movie_wlinks.objects.get(movie_id=movie_id)
		movie_id=movies.objects.get(id=movie_id)
	except Exception, e:
		output = 'wtitle does not exists for this movie add in admin'
		return render_to_response('wiki_parser/templates/parse_cast.html',
								{
									'output':output,
								},context_instance=RequestContext(request)
							)


	output2 = []
	f = urllib2.urlopen("""http://en.wikipedia.org/w/api.php?action=query&prop=links&titles="""+str(wtitle.wtitle)+"""&pllimit=5000&format=json""")
	a=json.loads(f.read())
	links =  a['query']['pages'][a['query']['pages'].keys()[0]]['links']
	output=''
	index=0
	for link in links:
		outputx = {}
		outputx['matches']=[]
		output = output+str(link['title'])
		outputx['link']=str(link['title'])
		query_obj= re.split(r' ',str(link['title']))
		for query_obj_item in query_obj:
			if len(query_obj_item) > 2:
				try:
					a=movie_persons.objects.filter(Q(person_name__icontains=query_obj_item))
				except Exception, e:
					pass
				if a!=[]:
					
					for item in a:
						output = output+"----"+"<a href='/person/"+(str(item.id))+"/index/'>"+item.person_name+"</a>"
						outputx['matches'].append(item)
				else:
					output = output + "none"
			outputx['matches']=list(set(outputx['matches']))
		output = output+'\n<br/>'

		output2.append(outputx)
	output = output+'<hr/>'
		
	return render_to_response('wiki_parser/templates/parse_cast.html',
								{
									'output':output,
									'results':output2,
									'movie':movie_id,
								},context_instance=RequestContext(request)
							)
import json
from movie_casting.models import movie_casting
from professions.models import professions

def parser_ajaxlink(request):
	ajaxresponse = {}

	if request.user.is_staff: 							
		pass
	else:
		ajaxresponse['message'] = 'user not staff to do this'
		ajaxresponse = json.dumps(ajaxresponse)
		return HttpResponse(ajaxresponse)
	
	try:
		person_id=request.GET['person_id']
		movie_id = request.GET['movie_id']
		movie_name = movies.objects.get(id=movie_id)
		person_name = movie_persons.objects.get(id=person_id)
	except Exception, e:
		ajaxresponse['message'] = 'ERROR'
		ajaxresponse = json.dumps(ajaxresponse)
		return HttpResponse(ajaxresponse)
	try:
		res=movie_casting.objects.filter(movie_id=movie_id,movie_persons_id=person_id)
		if res:
			ajaxresponse['message'] = '<strong>'+movie_name.movie_name+'</strong> and <strong>'+person_name.person_name+'</strong> are already linked'
			ajaxresponse = json.dumps(ajaxresponse)
			return HttpResponse(ajaxresponse)
	except Exception, e:
		pass

	new_cast = movie_casting()
	
	new_cast.movie_persons_id=person_name
	new_cast.movie_id=movie_name
	try:
		prof_id=professions.objects.filter(profession_name='unknown')[0]
	except Exception, e:
		print e
	
	new_cast.profession_id=prof_id
	try:
		new_cast.save()
	except Exception, e:
		ajaxresponse['message'] = 'unable to save probably already exists'
		ajaxresponse = json.dumps(ajaxresponse)
		return HttpResponse(ajaxresponse)

	ajaxresponse['person_id'] = person_id
	ajaxresponse['movie_id'] = movie_id

	ajaxresponse['message'] = '<strong>'+movie_name.movie_name+'</strong> and <strong>'+person_name.person_name+'</strong> with profession <strong>'+prof_id.profession_name+'('+str(prof_id.profession_level)+')'+'</strong> linked'

	print ajaxresponse['message']
	ajaxresponse = json.dumps(ajaxresponse)
	return HttpResponse(ajaxresponse)

def movie_parse_fields(request,movie_id):
	if request.user.is_staff:
		pass
	else:
		output = "user not staff"
		return render_to_response('wiki_parser/templates/parse_fields.html',
								{
									'output':output,
								},context_instance=RequestContext(request)
							)
	try:
		wtitle=movie_wlinks.objects.get(movie_id=movie_id)
	except Exception, e:
		output = 'wtitle does not exists for this movie add in admin'
		return render_to_response('wiki_parser/templates/parse_fields.html',
								{
									'output':output,
								},context_instance=RequestContext(request)
							)
	output =''
	f = urllib2.urlopen("""http://en.wikipedia.org/w/api.php?action=query&prop=revisions&rvprop=content&format=json&titles="""+str(wtitle.wtitle)+"""&rvsection=0&pllimit=5000""")
	a=json.loads(f.read())	
	infobox = a['query']['pages'][a['query']['pages'].keys()[0]]['revisions']
	info = re.split(r'\\n\|',str(infobox[0]))
	index=0
	while(index<len(info)):
		output = output+str(info[index])+'<br/>'
		index=index+1
	output = output+'<hr/>'
	return render_to_response('wiki_parser/templates/parse_fields.html',
								{
									'output':output
								},context_instance=RequestContext(request)
							  )


from bs4 import BeautifulSoup
from bs4 import UnicodeDammit

def person_parser_add(request):
	outputx={}
	if request.user.is_staff:
		pass
	else:
		output = "user not staff"
		return render_to_response('wiki_parser/templates/person_parser_add.html',
								{
									'output':output,
								},context_instance=RequestContext(request)
								)
	output = ''
	try:
		wlink=request.GET['wlink']
	except Exception, e:
		output='no link in url'
		return render_to_response('wiki_parser/templates/person_parser_add.html',
								{
									'output':output,
								},context_instance=RequestContext(request)
								)

	url = """http://en.wikipedia.org/w/api.php?action=parse&page="""+str(wlink)+"""&format=json&prop=text&section=0"""#
	url = re.sub(' ','_',url)
#	print url
	f = urllib2.urlopen(url)
	a=json.loads(f.read())	
	output = a['parse']['text']['*']
	soup = BeautifulSoup(output)
	text=''
	for ptext in soup.find_all('p'):
		text += ptext.get_text()
	dammit = UnicodeDammit(text)
	output = dammit.unicode_markup
	output = re.sub('Cite error: There are <ref> tags on this page, but the references will not show without a {{Reflist}} template or a <references /> tag; see the help ','',output)
	outputx['info']=output
	url="""http://en.wikipedia.org/w/api.php?action=query&titles="""+str(wlink)+"""&generator=images&gimlimit=10&prop=imageinfo&iiprop=url&format=json"""
	url = re.sub(' ','_',url)
#	print url
	f = urllib2.urlopen(url)
	a=json.loads(f.read())	
	wimages=a['query']['pages']
	output = output+'<br/>'
	images=[]
	for image in wimages:
		if int(image)>0:
			pass
		else:
			output = output + '<a href=\''+str(wimages[image]['imageinfo'][0]['url'])+'\' target=\'_blank\'><img src=\''+str(wimages[image]['imageinfo'][0]['url'])+'\' height=\'100px\' /></a>'
			images.append(str(wimages[image]['imageinfo'][0]['url']))
	outputx['images']=images
	return render_to_response('wiki_parser/templates/person_parser_add.html',
								{
									'output':output,
									'results':outputx,
								},context_instance=RequestContext(request)
							 )