from django.conf.urls import patterns, include, url

urlpatterns = patterns('indexv2.views',

	 url(r'^api/m/(?P<movie_id>\d+)/$','v2_m_info',name='v2_m_info'),
	 url(r'^api/m/cast/(?P<movie_id>\d+)/$','v2_m_info_cast',name='v2_m_info_cast'),
	# url(r'^api/p/(?P<person_id>\d+)/$','v2_p_info',name='v2_p_info'),
	
	url(r'^$','v2',name='v2'),

	url(r'^api/latestdb/$','latestdb',name='latestdb'),
	url(r'^api/releasing/$','releasing',name='releasing'),
	url(r'^api/intheaters/$','intheaters',name='intheaters'),

	url(r'^api/searching/$','searching',name='searching'),

	
)


