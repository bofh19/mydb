from django.db import models

# Create your models here.
from movies.models import movies
from movie_persons.models import movie_persons
from professions.models import professions

class movie_characters(models.Model):
	character_name=models.CharField(max_length=100)
	movie_id = models.ForeignKey(movies)
	movie_persons_id=models.ForeignKey(movie_persons)
	
	class Meta:
		unique_together=('movie_id','movie_persons_id')
	def __unicode__(self):
		return self.character_name+" - "+self.movie_id.movie_name+" - "+self.movie_persons_id.person_name