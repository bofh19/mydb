# Create your views here.
from django.shortcuts import render_to_response
from movie_persons.models import movie_persons
from movie_casting.models import movie_casting
from movies.models import movies
from images.models import images
from images.models import images_tags
import json, random
from django.shortcuts import get_object_or_404
from django.template import RequestContext
from django.http import HttpResponse
from django.utils.datastructures import MultiValueDictKeyError
from django.http import HttpResponseRedirect,HttpResponse
from django.core.urlresolvers import reverse
from images.models import profile_images
from professions.models import professions
from django.shortcuts import get_object_or_404
from images.models import banner_images
from movie_persons.models import alternate_names
from movie_persons.models import extra_info

def person_detail_index(request,person_id):
	name=get_object_or_404(movie_persons,id=person_id)

	try:
		this_person_professionz=movie_casting.objects.filter(movie_persons_id=person_id)
		this_person_professions_id=[]
		this_person_professions=[]
		for a in this_person_professionz:
			this_person_professions_id.append(a.profession_id)
			this_person_professions_id=list(set(this_person_professions_id))
		for a in this_person_professions_id:
			this_person_professions.append(professions.objects.get(id=a.id))
	except Exception, e:

		this_person_professions=''
	try:
		profile_imagez=profile_images.objects.filter(person_id=person_id,image_choosen='T')
		profile_image=random.choice(profile_imagez)
		random_profile_image=''
	except Exception, e:
		profile_image=''
		random_profile_image=''
		return render_to_response('movie_persons/templates/person_detail_info.html',
								{
									'name':name,								
									'profile_image':profile_image,
									'random_profile_image':random_profile_image,
									'person_professions':this_person_professions,
								},context_instance=RequestContext(request)
								)
	try:
		this_alternate_names=alternate_names.objects.filter(person_id=person_id)
	except Exception, e:
		print e
		this_alternate_names=''
	
	try:
		moviez=movie_casting.objects.filter(movie_persons_id=person_id).order_by( 'movie_id' )
		moviez=list(set(moviez))
		moviez=moviez[:5]
	except Exception, e:
		moviez=[]
	person_movies=[]

	#print moviez
	for each_movie in moviez:
		movie_json={}
		movie_json['movie']=movies.objects.get(id=each_movie.movie_id.id)
		try:
			#print each_movie.movie_id.id
			bimg=banner_images.objects.filter(movie_id=each_movie.movie_id.id)
			movie_json['back_image']=random.choice(bimg)
		except Exception, e:
			movie_json['back_image']=''
		person_movies.append(movie_json)

	#print person_movies
	return render_to_response('movie_persons/templates/person_detail_index.html',
							{
							'name':name,
							'profile_image':profile_image,
							'person_professions':this_person_professions,
							'person_movies':person_movies,
							'alternate_names':this_alternate_names
							},context_instance=RequestContext(request)
							)
from movie_trailers.models import movie_trailers

def person_detail_trailers(request,person_id):
	name=get_object_or_404(movie_persons,id=person_id)
	this_person_trailers=[]
	try:
		moviez=movie_casting.objects.filter(movie_persons_id=person_id)
		this_person_movies=list(set(moviez))
	except Exception, e:
		this_person_trailers=[]
		this_person_movies=[]

	for each_movie in this_person_movies:
		each_movie_trailers=movie_trailers.objects.filter(movie_id=each_movie.movie_id)
		for each_movie_trailer in each_movie_trailers:
			this_person_trailers.append(each_movie_trailer)
	this_person_trailers=this_person_trailers
	return render_to_response('movie_persons/templates/person_detail_trailers.html',
							{	
							'name':name,	
							'trailers':this_person_trailers,
							},context_instance=RequestContext(request)
							)
def person_detail_info(request, person_id):
	try:
		name=movie_persons.objects.get(id=person_id)
	except Exception, e:
		return render_to_response('movie_persons/templates/person_detail_info.html',
							{
								'name':'Does Not Exist',								
							},context_instance=RequestContext(request)
							)
	moviez=movie_casting.objects.filter(movie_persons_id=person_id)

############################## PROFILE IMAGE ####
	try:
		profile_imagez=profile_images.objects.filter(person_id=person_id,image_choosen='T')
		profile_image=random.choice(profile_imagez)
		random_profile_image=''
	except Exception, e:
		profile_image=''
		try:
			tempx=random.choice(images_tags.objects.filter(person_id=person_id))
			random_profile_image=images.objects.get(id=tempx.image_id.id)
		except Exception, e:
			#print e 
			random_profile_image=''

############################## PERSON MAIN CAST TYPES
	try:
		this_person_professionz=movie_casting.objects.filter(movie_persons_id=person_id)
		this_person_professions_id=[]
		this_person_professions=[]
		for a in this_person_professionz:
			this_person_professions_id.append(a.profession_id)
			this_person_professions_id=list(set(this_person_professions_id))
		for a in this_person_professions_id:
			this_person_professions.append(professions.objects.get(id=a.id))
	except Exception, e:

		this_person_professions=''
	extra_infos=''
	try:
		extra_infos=extra_info.objects.filter(person_id=person_id)
	except Exception, e:
		extra_infos=''
	return render_to_response('movie_persons/templates/person_detail_info.html',
							{
								'name':name,								
								'profile_image':profile_image,
								'random_profile_image':random_profile_image,
								'person_professions':this_person_professions,
								'extra_infos':extra_infos,
							},context_instance=RequestContext(request)
							)

def person_detail_movies(request, person_id):
	try:
		name=movie_persons.objects.get(id=person_id)
	except Exception, e:
		return render_to_response('movie_persons/templates/person_detail_info.html',
							{
								'name':'Does Not Exist',								
							},context_instance=RequestContext(request)
							)
	try:
		moviez=movie_casting.objects.filter(movie_persons_id=person_id).order_by( 'movie_id' )
		moviez_id=[]
		for a in moviez:
			moviez_id.append(a.movie_id.id)
			moviez_id=list(set(moviez_id))
	except Exception, e:
		return render_to_response('movie_persons/templates/person_detail_movies.html',
							{
								'name':name,								
							},context_instance=RequestContext(request)
							)
	result=[]
	for each_movie in moviez_id:
		movie_json={}
		movie_professions=[]
		try:
			movie_json['name']=movies.objects.get(id=each_movie).movie_name
			movie_json['id']=each_movie
		except Exception, e:
			pass
		
		try:
			movie_json['banner_image']=random.choice(banner_images.objects.filter(movie_id=each_movie))
		except Exception, e:
			movie_json['banner_image']=''

		for z in movie_casting.objects.filter(movie_persons_id=person_id,movie_id=each_movie):
			movie_profession={}
			movie_profession['id']=z.profession_id.id
			movie_profession['name']=z.profession_id.profession_name
			movie_professions.append(movie_profession)
		movie_json['professions']=movie_professions
		result.append(movie_json)
	person_movies=result
	return render_to_response('movie_persons/templates/person_detail_movies.html',
							{
								'global_var':global_var,
								'name':name,
								'person_movies':person_movies,								
							},context_instance=RequestContext(request)
							)
	






#
#
#
#
#
#
#

def person_searching(request):
	added_person=''
	if request.is_ajax():
		q=request.GET.get('term','')
		persons=movie_persons.objects.filter(person_name__icontains=q)[:20]
		results=[]
		for person in persons:
			person_json={}
			person_json['id']=person.id
			person_json['label']=person.person_name
			person_json['value']=person.person_name
			results.append(person_json)
		data=json.dumps(results)
	else:
		data='fail'
	mimetype='application/json'
	return HttpResponse(data, mimetype)
#
#
#
#
#
#
#
#

from movie_persons.forms import addmovie_persons
from django.contrib.auth.models import User
from images.models import profile_images

from forms import edit_person_form
from index import global_var
def edit_person(request,person_id):
	#verifying is staff status here
	if request.user.is_authenticated():
		if request.user.is_staff:
			username=request.user.username
		else:
			return HttpResponse("user not staff click back on browser to go back")
	else:
		return HttpResponse("""login before to <a href='/accounts/login/>here</a>""")
	updated_person=''
	if request.user.is_authenticated():
		username=request.user.username
	else:
		return HttpResponse("login to do this")

	try:
		person_name=movie_persons.objects.get(id=person_id)
	except Exception, e:
		return HttpResponse("invalid request")
	try:
		this_person_images=profile_images.objects.filter(person_id=person_id)
	except Exception, e:
		this_person_images=''
	if request.method=='POST':
		if request.user.is_staff:
			user_id=request.user.id
		else:
			return HttpResponseRedirect("/person/"+str(person_id)+"/info/?alert=User Does not have enough privilages to do this")
		form=edit_person_form(request.POST,request.FILES)
		
		if form.is_valid():
			this_person=movie_persons.objects.get(id=person_id)
			this_person.person_bio=form.cleaned_data['person_bio']
			try:
				if request.FILES['imagefile']:
					try:
						newimage=profile_images(imagefile=request.FILES['imagefile'])
					except Exception, e:
						return HttpResponse(e)
					newimage.user_id=user_id
					newimage.image_choosen='T'
					newimage.person_id=person_name
					newimage.user_level=9
					try:
						newimage.save()
						this_person.save()
					except Exception, e:
						pass
				updated_person=this_person.person_name+" updated and new image saved"
			except Exception, e:
				this_person.save()
				updated_person = this_person.person_name+" updated - No image saved"
			person_name2="-"+person_name.person_name
			return HttpResponseRedirect("/person/"+str(person_id)+str(person_name2)+"/info/?alert="+str(updated_person))
			#return HttpResponseRedirect(reverse('movie_persons.views.person_detail_info',  args=(person_id,person_name2)))
	else:
		form=edit_person_form(initial = {"person_date_of_birth":person_name.person_date_of_birth,
										"person_bio":person_name.person_bio,
											})
	return render_to_response('movie_persons/templates/edit_person.html',
									{'form':form,
									'person_name':person_name,
									'person_id':person_id,
									'images':this_person_images,
									'updated_person':updated_person,
									'global_var':global_var,
									},
									context_instance=RequestContext(request))

def enter_person(request):
	#verifying is staff status here
	if request.user.is_authenticated():
		if request.user.is_staff:
			username=request.user.username
		else:
			return HttpResponse("user not staff click back on browser to go back")
	else:
		return HttpResponse("""login before to <a href=/user/login/>login here</a>""")

	added_person=''
	if request.method=='POST':
		if request.user.is_authenticated():
			username=request.user.username
			users=request.user
		else:
			return HttpResponse("""login before to tag <a href=/movie/index/>login here</a>""")

		f=addmovie_persons(request.POST,request.FILES)
		user_id=request.user.id
		if f.is_valid():
			newperson=movie_persons(person_name=f.cleaned_data['person_name'])
			newperson.person_gender=f.cleaned_data['person_gender']
			try:
				newperson.person_date_of_birth=f.cleaned_data['person_date_of_birth']
			except:
				pass
			newperson.person_bio=f.cleaned_data['person_bio']
			try:
				person_sp=f.cleaned_data['person_sp']
			except Exception, e:
				pass
			try:
				newperson.save()
			except Exception, e:
				#print e
				return HttpResponse(e)
			try:
				if request.FILES['imagefile']:
					try:
						newimage=profile_images(imagefile=request.FILES['imagefile'])
					except Exception, e:
						newperson.delete()
						return HttpResponse(e)
					newimage.user_id=user_id
					newimage.image_choosen='T'
					newimage.person_id=newperson
					newimage.user_level=9
					try:
						newimage.save()
					except Exception, e:
						#print e
						newperson.delete()
				added_person=newperson.person_name
			except Exception, e:
				#print e
				added_person = newperson.person_name+" - No image saved"
	else:
		f=addmovie_persons()
	return render_to_response("movie_persons/templates/enter_person.html",
									{
									'form':f,
									'added_person':added_person,
									},
									context_instance=RequestContext(request))


###

#
#
#
#

#
#

def movie_persons_listall(request):
	from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
	items_per_page=30
	if request.user.is_authenticated():
		last_login = request.session.get('social_auth_last_login_backend')
	else:
		last_login=''
	
	try:
		if request.GET['p']:
			page_number=int(request.GET['p'])
		else:
			page_number=1
	except Exception, e:
		page_number=1

	page=page_number
	
	all_persons=movie_persons.objects.all()[::-1]
	paginator = Paginator(all_persons,items_per_page)

	try:
		all_persons = paginator.page(page)
	except PageNotAnInteger:
		all_persons = paginator.page(1)
	except EmptyPage:
		all_persons = paginator.page(paginator.num_pages)
	persons_list=[]

	for this_person in all_persons:
		person_json={}
		person_json['person']=this_person
		
		try:
			this_person_pimg=random.choice(profile_images.objects.filter(person_id=this_person.id))
			person_json['profile_image']=this_person_pimg
			print str(this_person.id) + this_person.person_name
			print this_person_pimg.imagefile.url
		except Exception, e:
			print e			
			person_json['profile_image']=''
		
		persons_list.append(person_json)
	return render_to_response('movie_persons/templates/persons_list_all.html',
								{
								'last_login':last_login,
								'all_persons':all_persons,
								'persons_list':persons_list,
								},context_instance=RequestContext(request)
							)
