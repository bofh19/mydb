from django import forms
from django.contrib.admin import widgets 
import datetime
from movie_persons.models import movie_persons
class addmovie_persons(forms.Form):
	GENDER_CHOICES = (
		(u'M',u'Male'),
		(u'F',u'Female'),
		(u'G',u'Gay'),
		)
	person_name=forms.CharField(max_length=400)
	person_gender=forms.ChoiceField(choices=GENDER_CHOICES)
	person_date_of_birth=forms.DateField(required=False,initial='1800-01-01',widget=widgets.AdminDateWidget)
	person_bio=forms.CharField(max_length=2000)
	person_sp=forms.CharField(max_length=40,required=False,initial="Unknown")
	imagefile=forms.ImageField(label='select an image',help_text='max 42 megabytes',required=False)

	def clean_person_name(self):
		person_name = self.cleaned_data['person_name']
		test_person=movie_persons()
		try:
			test_person=movie_persons.objects.get(person_name=person_name)
		except test_person.DoesNotExist:
			return person_name
		raise forms.ValidationError(u'%s already exists' % person_name)

class edit_person_form(forms.Form):
	person_date_of_birth=forms.DateField(required=False,initial='1800-01-01',widget=widgets.AdminDateWidget)
	person_bio=forms.CharField(max_length=2000,widget=forms.Textarea)
	imagefile=forms.ImageField(label='select an image',help_text='max 42 megabytes',required=False)