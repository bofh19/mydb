from django.db import models

# Create your models here.
from movies.models import movies
class movie_news(models.Model):
	movie_id=models.ForeignKey(movies)
	news_source=models.CharField(max_length=500,blank=True,null=True)
	news=models.CharField(max_length=5000,blank=True,null=True)
	TYPE_CHOICES = (
		(u'I',u'Image'),
		(u'R',u'Review'),
		(u'T',u'Text'),
		(u'V',u'Video'),
		)
	news_type=models.CharField(max_length=2,choices=TYPE_CHOICES)

	def __unicode__(self):
		return self.movie_id.movie_name+self.news_type