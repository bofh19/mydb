# Create your views here.
from django.shortcuts import render_to_response
from django.shortcuts import get_object_or_404
from django.template import RequestContext

from movies.models import movies
from movie_news.models import movie_news

from itertools import chain
from index import global_var

def movie_detail_news(request,movie_id):
	if request.user.is_authenticated():
		last_login=request.session.get('social_auth_last_login_backend')
	else:
		last_login=''
	try:
		movie_name=get_object_or_404(movies,id=movie_id)
	except UnboundLocalError:
		movie_name="Does Not Exist"		
	except movies.DoesNotExist:
		movie_name="Does Not Exist"
		
	try:
		this_movie_news1=movie_news.objects.filter(movie_id=movie_id).exclude(news_type='R').exclude(news_type='T').order_by('-id')
	except Exception, e:
		this_movie_news1=[]

	try:
		this_movie_news2=movie_news.objects.filter(movie_id=movie_id).exclude(news_type='I').exclude(news_type='V').order_by('-id')
	except Exception, e:
		this_movie_news2=[]
	this_movie_news=list(chain(this_movie_news1,this_movie_news2))
	a_response={
				'movie_id':movie_id,
				'name':movie_name,
				'movie_news':this_movie_news,
				}

	
	return render_to_response('movie_news/templates/movie_detail_news.html',
									a_response,
								context_instance=RequestContext(request)
								)

def news_detail(request,news_id):
	this_news=''
	this_news=get_object_or_404(movie_news,id=news_id)
	a_response={
		'news':this_news,
		'global_var':global_var,
	}
	return render_to_response('movie_news/templates/news_detail.html',

									a_response,
								context_instance=RequestContext(request)
								)	