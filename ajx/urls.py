from django.conf.urls import patterns, include, url

urlpatterns = patterns('ajx.views',
	url(r'^minfojson/(?P<movie_id>\d+)/$','api_movie_info',name='api_movie_info'),
	url(r'^minfoframe/(?P<movie_id>\d+)/$','frame_movie_info',name='frame_movie_info'),

	url(r'^pinfojson/(?P<person_id>\d+)/$','api_person_info',name='api_person_info'),
	url(r'^pinfoframe/(?P<person_id>\d+)/$','frame_person_info',name='frame_person_info'),
)


