from django.http import HttpResponse
from urlparse import urlparse
from django.shortcuts import render_to_response
from django.template import RequestContext
from images.models import background_images
from movie_music.models import *
from movies.models import movies

from movie_trailers.models import movie_trailers
from images.models import banner_images
from movie_genres.models import movie_genres
from genres.models import genres
from movie_persons.models import movie_persons
from movie_casting.models import movie_casting
from professions.models import professions
from movies.models import movie_theater_status
from images.models import profile_images

import random
import json
import re

from datetime import *
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.csrf import requires_csrf_token
from django.views.decorators.csrf import ensure_csrf_cookie

# movie_id=7
# url= "http://127.0.0.1:8000/api/mif/"+movie_id+"/"
# $.getJSON(url,function(data){console.log(data)})


from sorl.thumbnail import get_thumbnail
MOVIE_PERSON_THUMBS_SIZE = '40x40'
MOVIE_BANNER_SIZE = '200x300'




@ensure_csrf_cookie
@requires_csrf_token
@csrf_protect
def frame_movie_info(request,movie_id):
	return render_to_response('ajx/templates/frame_movie_info.html',
									{
									'movie_id': movie_id
									},context_instance=RequestContext(request)
								)
		



@ensure_csrf_cookie
@requires_csrf_token
@csrf_protect
def api_movie_info(request,movie_id):
	if request.is_ajax():
		result = {}
		mimetype='application/json'

		movie_json={}

		try:
			movie=movies.objects.get(id=movie_id)
		except Exception, e:
			result_json='none'
			return HttpResponse(result_json,mimetype)

		movie_json['id']=movie.id
		movie_json['name']=movie.movie_name

		genre_json=[]
		try:
			genres_all=movie_genres.objects.filter(movie_id=movie.id)
		except UnboundLocalError:
			genres_all=''
		except movie_genres.DoesNotExist:
			genres_all=''

		for genre in genres_all:
			genre_json.append(genres.objects.get(id=genre.genre_id.id).genre_name)
			genre_json=list(set(genre_json))

		movie_json['genres']=genre_json

		now=date.today()
		diff=now-movie.movie_year
		movie_json['days_left']=diff.days

		date_json={}

		date_json['year']=movie.movie_year.year
		date_json['day']=movie.movie_year.day
		date_json['month']=movie.movie_year.month
		date_json['weekday']=movie.movie_year.weekday()

		movie_json['date']=date_json
		try:
			movie_json['status']=movie_theater_status.objects.get(movie_id=movie_id).release_status
		except Exception, e:
			movie_json['status']=''

		movie_json['info']=movie.movie_plot
		try:
			movie_plot_x=movie.movie_plot.replace('<p>','')
			movie_plot_x=movie_plot_x.replace('</p>','')
			movie_json['info_small']=movie_plot_x[:190]
		except Exception, e:
			movie_json['info_small']=movie.movie_plot[:190]
		
		try:
			movie_per=[]
			movie_pers=[]

			prof_id=professions.objects.get(profession_name='Hero')
			this_movie_persons=movie_casting.objects.filter(movie_id=movie.id,profession_id=prof_id.id)
			
			for person in this_movie_persons:
				movie_per.append( movie_persons.objects.get(id=person.movie_persons_id.id) )

			prof_id=professions.objects.get(profession_name='Heroine')
			this_movie_persons=(movie_casting.objects.filter(movie_id=movie.id,profession_id=prof_id.id))
			for person in this_movie_persons:
				movie_per.append( movie_persons.objects.get(id=person.movie_persons_id.id) )

			prof_id=professions.objects.get(profession_name='Director')
			this_movie_persons=(movie_casting.objects.filter(movie_id=movie.id,profession_id=prof_id.id))
			for person in this_movie_persons:
				movie_per.append( movie_persons.objects.get(id=person.movie_persons_id.id) )

			prof_id=professions.objects.get(profession_name='Music Director')
			this_movie_persons=(movie_casting.objects.filter(movie_id=movie.id,profession_id=prof_id.id))
			for person in this_movie_persons:
				movie_per.append( movie_persons.objects.get(id=person.movie_persons_id.id) )

			prof_id=professions.objects.get(profession_name='Producer')
			this_movie_persons=(movie_casting.objects.filter(movie_id=movie.id,profession_id=prof_id.id))
			 		
			for person in this_movie_persons:
				movie_per.append( movie_persons.objects.get(id=person.movie_persons_id.id) )

			movie_per=list(set(movie_per))

			for person in movie_per:
				person_json={}
				person_json['id']=person.id
				person_json['name']=person.person_name
				try:
					this_person_profile_image = random.choice(profile_images.objects.filter(person_id=person.id,image_choosen='T'))
					person_json['profile_image']=this_person_profile_image.imagefile.url
					person_json['profile_image_small'] = get_thumbnail(this_person_profile_image.imagefile, MOVIE_PERSON_THUMBS_SIZE, crop='top').url
				except Exception, e:
					person_json['profile_image']=''
				movie_pers.append(person_json)
			
			movie_json['persons']=movie_pers
		except Exception, e:
			movie_json['persons']=''

		try:
			this_trailers=movie_trailers.objects.filter(movie_id=movie.id).values('youtube_code')[::-1]
			if this_trailers:
				movie_json['trailers']=this_trailers
			else:
				movie_json['trailers']=''
		except Exception, e:
			movie_json['trailer']=''

		try:
			this_movie_banner_images=banner_images.objects.filter(movie_id=movie.id)
			this_movie_banner_image=random.choice(this_movie_banner_images)
			movie_json['banner_image']=this_movie_banner_image.imagefile.url
			im = get_thumbnail(this_movie_banner_image.imagefile, MOVIE_BANNER_SIZE, crop='top')
			movie_json['banner_image_small']=im.url
			
		except banner_images.DoesNotExist:
			movie_json['banner_image']=''
		except Exception, e:
			movie_json['banner_image']=''

		result = movie_json
		result_json=json.dumps(result)
		return HttpResponse(result_json,mimetype)
	return HttpResponse('fail')