from user_fav.models import user_fav_movies, user_fav_movie_persons
from django.contrib import admin

admin.site.register(user_fav_movie_persons)
admin.site.register(user_fav_movies)