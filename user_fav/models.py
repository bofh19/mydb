from django.db import models

# Create your models here.

from movies.models import movies
from movie_persons.models import movie_persons
from django.contrib.auth.models import User

class user_fav_movies(models.Model):
	user_id=models.ForeignKey(User)
	movie_id=models.ForeignKey(movies)

	class Meta:
		unique_together=('user_id','movie_id')

	def __unicode__(self):
		output = self.user_id.username + " " + self.movie_id.movie_name
		return output

class user_fav_movie_persons(models.Model):
	user_id=models.ForeignKey(User)
	movie_person_id=models.ForeignKey(movie_persons)

	class Meta:
		unique_together=('user_id','movie_person_id')